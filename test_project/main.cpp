/*
License under zlib license
Copyright (C) 2020 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<SDL2/SDL.h>

#include "base_app.h"
#include "base_graphics.h"
#include "base_math.h"

using std::cout;
using std::endl;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern int window_width;
extern int window_height;
extern int logical_screen_width;
extern int logical_screen_height;

v2 pivot;
Box *box_a;
Box *box_b;
Box *vertex;
Box *origin;

Circle *circle_a;
Circle *circle_b;

v2 *sum_list_cp;

TTF_Font *ubuntu_reguler;

void on_init(){

  ubuntu_reguler = load_font(FONTS_PATH + "Ubuntu-R.ttf", 16);
  TTF_SetFontStyle(ubuntu_reguler, TTF_STYLE_BOLD);

  pivot.x = vpos(logical_screen_width) / 2; 
  pivot.y = vpos(logical_screen_height) / 2;

  box_a = create_box(pivot, 5.0f, 5.0f); 
  box_b = create_box(v2{pivot.x, 20.0f}, 5.0f, 5.0f); 
  vertex = create_box(pivot, 0.7f, 0.7f); 

  origin = create_box(
		      v2{box_b->position.x - 9.0f,
			 box_b->position.y},
		      0.7f, 0.7f);

  sum_list_cp = (v2*)malloc( 4 * 4 * sizeof(v2) );

  circle_a = create_circle(v2{pivot.x - 15, pivot.y}, 3.5f);
  circle_b = create_circle(v2{pivot.x - 15, pivot.y - 20}, 3.5f);


}

void on_update(){
 
  v2 mouse_position = {vpos(event.motion.x),
                       vpos(event.motion.y)};
 
  // is_intersect(box_a, box_b, sum_list_cp, origin->position);
 
  if(is_intersect_aabb(box_a, box_b)){
    cout<<"intersect..."<<endl;
  }
 
  if(is_intersect(box_b, mouse_position)){
    cout<<"point to box intersect..."<<endl;
  }
 
  if(is_intersect(circle_a, circle_b)){
    cout<<"circle intersect..."<<endl;
  }
 
  // if(is_intersect(circle_a, mouse_position)){
  //   cout<<"point to circle intersect..."<<endl;
  // }
 
 // box_a->position = {mouse_position.x,
 //                    mouse_position.y};
 
 // circle_a->position = {mouse_position.x,
 //                       mouse_position.y};
 
 // if(sum_list_cp != NULL){
 //   for(int i = 0; i < 16; ++i){
 //     cout<<i<<", "<<sum_list_cp[i]<<endl;
 //   }
 // }

  circle_a->position = mouse_position;

  if(is_intersect(box_b, circle_a)){
    cout<<"circle and box intersect..."<<endl;
  }
 
}

void on_draw(){

  draw_text(renderer, 
            ubuntu_reguler,
            "howdy partner...",
            0,0,
            BLACK,
            WHITE);

  draw_box(renderer, box_b, GREEN);
  draw_box(renderer, box_a, BLUE);
  draw_box(renderer, origin, WHITE);
 
  draw_circle(renderer, circle_b, GREEN);
  draw_circle(renderer, circle_a, BLUE);
 
  // if(sum_list_cp != NULL){
  //   for(int i = 0; i < 16; ++i){
  //     vertex->position = sum_list_cp[i];
  //     draw_box(renderer, vertex, RED);
  //   }
  // }
}

void on_draw_gui(void){
}

void input_handler(void){
}

void on_clear(){
  TTF_CloseFont(ubuntu_reguler);
  ubuntu_reguler = NULL;
}

int main(){
  create_app(600, 600);
  on_init();

  run_app(on_update, 
          on_draw,
	  on_draw_gui,
          input_handler);

  on_clear();
  clear_app();
  SDL_Quit();
   
  return 0;
}
