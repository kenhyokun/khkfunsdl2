/*
License under zlib license
Copyright (C) 2020 - 2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<SDL2/SDL.h>
#include<vector>

#include "base_app.h"
#include "base_graphics.h"
#include "base_math.h"
#include "base_string.h"
#include "base_vector.h"

using std::cout;
using std::endl;
using std::string;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern int window_width;
extern int window_height;
extern int logical_screen_width;
extern int logical_screen_height;

Sprite *lilwitch_sprite;
SpriteAnimator *lilwitch_sprite_animator;
int run_right[4] = {1, 2, 3, 4};
int run_left[4] = {5, 6, 7, 8};

TTF_Font *ubuntu_reguler;

Tiledmap *tiledmap;

void on_init(){

  tiledmap = create_tiledmap(renderer, "../resources/lua/map2.lua");
  print_tiledmap_info(tiledmap);

  LuaState *LL = create_lua_state();
  load_lua_file(LL, "../resources/lua/tes.lua");
  cout<<get_lua_string(LL, "name")<<endl;
  cout<<get_lua_int(LL, "level")<<endl;
  cout<<get_lua_length(LL, "name")<<endl;

  // change name value in lua virtual machine
  lua_pushstring(LL, "kuncoro");
  lua_setglobal(LL, "name");
  lua_pop(LL, 1);

  cout<<get_lua_string(LL, "name")<<endl;

  lua_close(LL);

  Texture *lilwitch_texture =
    load_texture(renderer, "../resources/images/lilwitch.png");

  lilwitch_sprite = create_sprite(lilwitch_texture);

  lilwitch_sprite_animator = create_sprite_animator(lilwitch_sprite,
						    v2i{78, 87});

  fun::vector<int> fun_vec = create_vector<int>();
  fun_vec.push_back(32);
  fun_vec.push_back(42);
  fun_vec.push_back(89);
  fun_vec.push_back(55);
  fun_vec.push_back(23);
  fun_vec.push_back(67);
  fun_vec.push_back(99);
  fun_vec.push_back(32);

  cout<<"vector size:"<<fun_vec.size<<endl;
  fun_vec.erase(1);
  fun_vec.erase(1);
  cout<<"vector size:"<<fun_vec.size<<endl;
  cout<<"vector at 0 *:"<<*fun_vec.t[0]<<endl;
  cout<<"vector at 1 *:"<<*fun_vec.t[1]<<endl;
  cout<<"vector at (0):"<<fun_vec.at(0)<<endl;
  cout<<"vector at (1):"<<fun_vec.at(1)<<endl;
  cout<<"vector at [0]:"<<fun_vec[0]<<endl;
  cout<<"vector at [1]:"<<fun_vec[1]<<endl;
  cout<<"vector end:"<<*fun_vec.t[fun_vec.size - 1]<<endl;
  fun_vec.erase(1);
  cout<<"vector size:"<<fun_vec.size<<endl;
  // fun_vec.erase(1);
  // cout<<"vector size:"<<fun_vec.size<<endl;
  // fun_vec.erase(1);
  // cout<<"vector size:"<<fun_vec.size<<endl;
  cout<<"vector begin + 1:"<<**(fun_vec.begin() + 1)<<endl;
  cout<<"vector end - 1:"<<**(fun_vec.end() - 1)<<endl;

}

void on_update(){
 
  v2 mouse_position = {vpos(event.motion.x),
                       vpos(event.motion.y)};

  run_sprite_animator(lilwitch_sprite_animator,
		      run_right,
		      4,
		      5);
 
}

void on_draw(){
  SDL_SetRenderDrawColor(renderer, SKYBLUE.r, SKYBLUE.g, SKYBLUE.b, SKYBLUE.a);
  SDL_RenderFillRect(renderer, NULL);

  draw_tiledmap(renderer,
		tiledmap,
		0.0f,
		0.0f);

  draw_sprite_animator(renderer,
  		       lilwitch_sprite_animator,
   		       30.0f,
   		       30.0f);

}

void on_draw_gui(void){
}

void input_handler(void){
}

void on_clear(){
  TTF_CloseFont(ubuntu_reguler);
  ubuntu_reguler = NULL;
}

int main(){
  create_app(600, 600);
  on_init();

  run_app(on_update, 
          on_draw,
	  on_draw_gui,
          input_handler);

  on_clear();
  clear_app();
  SDL_Quit();
   
  return 0;
}
