/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<SDL2/SDL.h>
#include<vector>

#include "base_app.h"
#include "base_graphics.h"
#include "base_math.h"
#include "base_node.h"
#include "base_vector.h"
#include "base_string.h"

using std::cout;
using std::endl;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern int window_width;
extern int window_height;
extern int logical_screen_width;
extern int logical_screen_height;

void on_init(){
  Node *node1 = create_node("node1");
  Node *node2 = create_node("node2");
  Node *node3 = create_node("node3");
  Node *node4 = create_node("node4");
  Node *node5 = create_node("node5");

  // set_node_parent(node2, node1);
  // set_node_parent(node3, node1);
  // set_node_parent(node4, node1);

  // cout<<(**node1->child.begin())->name<<endl;
  // cout<<node1->child[0]->name<<endl;

  // cout<<node1->child.find(&node2)<<endl;
  // cout<<node1->child.find(&node3)<<endl;
  // cout<<node1->child.find(&node4)<<endl;

  // node1->child.erase(&node2);
  // cout<<node1->child.size<<endl;
  // node1->child.erase(&node4);
  // cout<<node1->child.size<<endl;
  // cout<<(**node1->child.begin())->name<<endl;

  add_node_child(node1, node2);
  add_node_child(node1, node3);
  add_node_child(node1, node4);

  add_node_child(node5, node2);

  cout<<node1->child.size<<endl;
  cout<<(**node1->child.begin())->name<<endl;
  cout<<(**node1->child.end())->name<<endl;
  cout<<node2->parent->name<<endl;


}

void on_update(){
 
  v2 mouse_position = {vpos(event.motion.x),
                       vpos(event.motion.y)};
}

void on_draw(){
  SDL_SetRenderDrawColor(renderer, SKYBLUE.r, SKYBLUE.g, SKYBLUE.b, SKYBLUE.a);
  SDL_RenderFillRect(renderer, NULL);
}

void on_draw_gui(void){
}

void input_handler(void){
}

void on_clear(){
}

int main(){
  create_app(600, 600);
  on_init();

  run_app(on_update, 
          on_draw,
	  on_draw_gui,
          input_handler);

  on_clear();
  clear_app();
  SDL_Quit();
   
  return 0;
}
