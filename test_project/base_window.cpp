/*
License under zlib license
Copyright (C) 2020-2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<SDL2/SDL.h>
#include<base_app.h>
#include<base_graphics.h>
#include<base_node.h>
#include<base_vector.h>

#include<random>

using std::cout;
using std::endl;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern int window_width;
extern int window_height;
extern int logical_screen_width;
extern int logical_screen_height;
extern long last_timer;

enum DirState{
	      right_dir,
	      left_dir,
	      up_dir,
	      down_dir,
};

void on_init(){
}

void on_update(){
}

void on_draw(){
  set_renderer_background_color(renderer, SKYBLUE);
}

void on_draw_gui(){
}

void on_clear(){
}

int main(){
  create_app(800, 800, "Base Window", true, "nearest");
  on_init();

  run_app(on_update, 
	  on_draw,
	  on_draw_gui,
	  60);

  on_clear();
  clear_app();
  SDL_Quit();

  return 0;
}
