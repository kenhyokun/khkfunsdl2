2D game framework with SDL2 library. <br/>
**(C++)(Functional programming)** <br/>
**Build and tested on:** <br/>
  **- Ubuntu 18.04 with gnu g++ compiler** <br/>
  **- macOS 10.13.6 with clang compiler** <br/>
  **- Windows10 64 bit with clang compiler(abandoned since it require silly Visual Studio linker settings)** <br/>
  **- Windows11 64 bit with msys mingw64** <br/>

# Features: 
  - Load exported **.lua** file from **[Tiledmap][1]** to create basic tiledmap. (only orthogonal view for now)
  - Load exported **.atlas** file from **[gdx texture packer][2]** to create sprite image.

# Third party library:
  - **[Lua][3]** **v5.4**
  - **[SDL2_gfx][4]** **v1.0.4**

# Dependencies:
  - SDL2
  - SDL2 TTF
  - SDL2 Image
  - SDL2 Mixer

# Todo for version 1.0.0 release:

#
<p>
Kevin Haryo Kuncoro <br/>
kevinhyokun91@gmail.com <br/>
2020-2021 
</p>

[1]: https://www.mapeditor.org/
[2]: https://github.com/crashinvaders/gdx-texture-packer-gui
[3]: https://www.lua.org/about.html
[4]: https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/
