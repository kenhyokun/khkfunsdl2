extension = {
  ".png", 
  ".ttf", 
  ".lua"
}

-- default resource directory
resources = "./resources/"

-- custom / additional resouce directory
custom_resources = "./custom_resources/lua/"
