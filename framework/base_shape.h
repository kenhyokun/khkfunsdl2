/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_SHAPE
#define BASE_SHAPE

#include<iostream>
#include<cstring>

#include "base_sdl2_include.h"
#include "base_math.h"

struct Rectangle{
  v2 position;
  float width;
  float height;
};

struct Circle{
  v2 position;
  float radius;
};

Rectangle* create_rectangle(v2 position, float width, float height);
Circle *create_circle(v2 position, float radius);

bool is_intersect(Rectangle *rectangle_a, Rectangle *rectangle_b) ;
bool is_intersect(Rectangle *rectangle , Circle *circle);
bool is_intersect(Rectangle *rectangle , v2 point);
bool is_intersect(Circle *circle_a, Circle *circle_b);
bool is_intersect(Circle *circle , v2 point);
bool is_intersect(SDL_Rect *rect_a , SDL_Rect *rect_b);
bool is_intersect(SDL_Rect *rect , v2i point);

#endif
