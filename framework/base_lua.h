/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_LUA
#define BASE_LUA

#include<iostream>
#include<lua.hpp>

using std::cout;
using std::endl;

// default lua function:
// lua_close(LuaState *L);

typedef lua_State LuaState;

LuaState *create_lua_state();
bool load_lua_file(LuaState *L, const char *path);
std::string get_lua_string(LuaState *L, const char *key);
const char* get_lua_cstr(LuaState *L, const char *key);
int get_lua_int(LuaState *L, const char *key);
int get_lua_length(LuaState *L, const char *key);

#endif
