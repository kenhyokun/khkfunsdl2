/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_shape.h"

Rectangle* create_rectangle(v2 position, float width, float height){
  Rectangle *rectangle = (Rectangle*)malloc(sizeof(Rectangle));
  rectangle->position = position;
  rectangle->width = width;
  rectangle->height = height;
  return rectangle;
}

Circle* create_circle(v2 position, float radius){
  Circle *circle = (Circle*)malloc(sizeof(Circle*));
  circle->position = position;
  circle->radius = radius;
  return circle;
}

struct AABB{
  v2 min;
  v2 max;
};

/*
  TODO[kevin]:
  AABB bounding rectangle size should be resized when rectangle rotate.
  we need to calculate min and max when rectangle rotate.
*/
BASE_SHAPE
AABB* create_aabb(Rectangle *rectangle){
  AABB *aabb = (AABB*)malloc(sizeof(AABB));
  float half_width = rectangle->width / 2.0f;
  float half_height = rectangle->height / 2.0f;

  // top left corner
  aabb->min = (v2){rectangle->position.x - half_width,
    rectangle->position.y - half_height};

  // bottom right corner
  aabb->max = (v2){rectangle->position.x + half_width,
    rectangle->position.y + half_height};

  return aabb;
}

BASE_SHAPE
AABB* create_aabb(SDL_Rect *rect){
  AABB *aabb = (AABB*)malloc(sizeof(AABB));

  // top left corner
  aabb->min = (v2){(float)rect->x, (float)rect->y};

  // bottom right corner
  aabb->max = (v2){(float)rect->x + (float)rect->w,
    (float)rect->y + (float)rect->w};

  return aabb;
}

bool is_intersect(Rectangle *rectangle_a, Rectangle *rectangle_b){
  AABB *aabb_a = create_aabb(rectangle_a);
  AABB *aabb_b = create_aabb(rectangle_b);
  float dx1 = aabb_b->min.x - aabb_a->max.x;
  float dy1 = aabb_b->min.y - aabb_a->max.y;
  float dx2 = aabb_a->min.x - aabb_b->max.x;
  float dy2 = aabb_a->min.y - aabb_b->max.y;

  free(aabb_a);
  free(aabb_b);
  aabb_a = nullptr;
  aabb_b = nullptr;

  if(dx1 > 0.0f || dy1 > 0.0f) return false;
  if(dx2 > 0.0f || dy2 > 0.0f) return false;

  return true;
}

bool is_intersect(Rectangle *rectangle, v2 point){
  AABB *aabb = create_aabb(rectangle);
  float dx1 = point.x - aabb->max.x;
  float dy1 = point.y - aabb->max.y;
  float dx2 = aabb->min.x - point.x;
  float dy2 = aabb->min.y - point.y;

  free(aabb);
  aabb = nullptr;

  if(dx1 > 0.0f || dy1 > 0.0f) return false;
  if(dx2 > 0.0f || dy2 > 0.0f) return false;

  return true;
}

bool is_intersect(Rectangle *rectangle, Circle *circle){
  v2 rectangle_half_size = {rectangle->width / 2, rectangle->height / 2};
  v2 difference = circle->position - rectangle->position;
  v2 clamped = clamp(difference, -rectangle_half_size, rectangle_half_size);
  v2 closest = rectangle->position + clamped;
  difference = closest - circle->position;

  bool intersect = get_magnitude(difference) < circle->radius;
  return intersect;
}

bool is_intersect(Circle *circle_a, Circle *circle_b){

  float distance = get_distance(circle_a->position,
				circle_b->position);

  float radius = circle_a->radius + circle_b->radius;
  bool intersect = distance < radius;
  return intersect;
}

bool is_intersect(Circle *circle, v2 point){
  float dx = circle->position.x - point.x;
  float dy = circle->position.y - point.y;
  float distance = sqrt( pow(dx, 2) + pow(dy, 2) );

  bool intersect = distance < circle->radius;
  return intersect;
}

bool is_intersect(SDL_Rect *rect_a , SDL_Rect *rect_b){
  AABB *aabb_a = create_aabb(rect_a);
  AABB *aabb_b = create_aabb(rect_b);
  float dx1 = aabb_b->min.x - aabb_a->max.x;
  float dy1 = aabb_b->min.y - aabb_a->max.y;
  float dx2 = aabb_a->min.x - aabb_b->max.x;
  float dy2 = aabb_a->min.y - aabb_b->max.y;

  free(aabb_a);
  free(aabb_b);
  aabb_a = nullptr;
  aabb_b = nullptr;

  if(dx1 > 0.0f || dy1 > 0.0f) return false;
  if(dx2 > 0.0f || dy2 > 0.0f) return false;
  return true;
}

bool is_intersect(SDL_Rect *rect , v2i point){
  AABB *aabb = create_aabb(rect);
  float dx1 = point.x - aabb->max.x;
  float dy1 = point.y - aabb->max.y;
  float dx2 = aabb->min.x - point.x;
  float dy2 = aabb->min.y - point.y;

  free(aabb);
  aabb = nullptr;

  if(dx1 > 0.0f || dy1 > 0.0f) return false;
  if(dx2 > 0.0f || dy2 > 0.0f) return false;

  return true;
}
