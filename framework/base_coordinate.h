/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_COORDINATE
#define BASE_COORDINATE

#include<iostream>
#include "base_math.h"

struct Viewport{
  int left;
  int right;
  int top;
  int bottom;
};

void Viewport_Init(int logical_width, int logical_height);
Viewport* create_viewport(int left, int right, int bottom, int top);
v2i get_screen_position(float pos_x, float pos_y);
v2 get_world_position(int pos_x, int pos_y);

#endif
