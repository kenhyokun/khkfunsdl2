/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_STRING
#define BASE_STRING

#include<iostream>
#include<string.h>

using std::size_t;

#define STRING_NPOS std::string::npos

namespace fun{

  struct string{
    const char *c_str;

    size_t length();
    char at(int index);
    char operator[] (const int &index);
    void operator= (const char *_c_str);
    void operator= (const fun::string &_string);
    void operator= (const std::string &_string);
    bool operator== (const char *_c_str);
    bool operator== (const fun::string &_string);
    bool operator== (const std::string &_string);
    bool operator!= (const char *_c_str);
    bool operator!= (const fun::string &_string);
    bool operator!= (const std::string &_string);
    void operator+= (const char *_c_str);
    void operator+= (const fun::string &_string);
    void operator+= (const std::string &_string);
    fun::string operator+ (const char *_c_str);
    fun::string operator+ (const fun::string &_string);
    fun::string operator+ (const std::string &_string);
    size_t find(char _char, size_t pos = 0);
    size_t find(const char *str, size_t pos = 0);
    char* substr(size_t pos, size_t len);
    void clear();

    inline friend std::ostream &operator<<(std::ostream &output, const fun::string &_string){
      output<< _string.c_str;
      return output;
    }

    // static function
    static char* alloc_string(const char* str);
    static void append(char *dst_str, const char *cat_str);
    static bool equal(const char *str1, const char *str2);
    static size_t find(const char *str, char _char, size_t pos = 0);
    static size_t find(const char *str1, const char *str2, size_t pos = 0);
    static char* substr(const char *str, size_t pos, size_t len);

  };

} // namespace

void create_string(fun::string *fun_string, const char *str);
fun::string create_string(const char* str);
fun::string create_string(size_t size);

#endif
