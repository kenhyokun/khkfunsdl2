/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_lua.h"

LuaState *create_lua_state(){
  return luaL_newstate();
}

bool load_lua_file(LuaState *L, const char *path){
  if(luaL_dofile(L, path) != LUA_OK){
    cout<<lua_tostring(L, -1)<<endl;
    return false;
  }
  return true;
}

std::string get_lua_string(LuaState *L, const char *key){
  std::string get_string = "";

  lua_getglobal(L, key);

  if( lua_isnil(L, -1) ){
    lua_pop(L, 1);

    lua_pushstring(L, key);
    lua_gettable(L, -2);
    get_string = lua_tostring(L, -1);
  }
  else{
    get_string = lua_tostring(L, -1);
  }

  lua_pop(L, 1);

  return get_string;
}

const char* get_lua_cstr(LuaState *L, const char *key){
  const char *get_string = "";

  lua_getglobal(L, key);

  if( lua_isnil(L, -1) ){
    lua_pop(L, 1);

    lua_pushstring(L, key);
    lua_gettable(L, -2);
    get_string = lua_tostring(L, -1);
  }
  else{
    get_string = lua_tostring(L, -1);
  }

  lua_pop(L, 1);

  return get_string;
}

int get_lua_int(LuaState *L, const char *key){
  int get_int = 0;

  lua_getglobal(L, key);

  if( lua_isnil(L, -1) ){
    lua_pop(L, 1);

    lua_pushstring(L, key);
    lua_gettable(L, -2);
    get_int = lua_tointeger(L, -1);
  }
  else{
    get_int = lua_tointeger(L, -1);
  }

  lua_pop(L, 1);

  return get_int;
}

int get_lua_length(LuaState *L, const char *key){
  int len = 0;

  lua_getglobal(L, key);

  if( lua_isnil(L, -1) ){
    lua_pop(L, 1);

    lua_pushstring(L, key);
    lua_gettable(L, -2);
    len = lua_rawlen(L, -1); 
  }
  else{
    len = lua_rawlen(L, -1); 
  }

  lua_pop(L, 1);

  return len;
}
