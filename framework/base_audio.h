/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_AUDIO
#define BASE_AUDIO

#include<iostream>
#include "base_sdl2_include.h"

typedef Mix_Chunk AudioSample;

using std::cout;
using std::endl;
using std::string;

enum AudioPlayMode{
  AudioPlayMode_loop = -1, // infinity loop
  AudioPlayMode_once = 0
  // +1 for twice
  // +2 for three times
  // +3 for four times
  // and so on...
};

struct AudioSource{
  AudioSample *sample;
  int channel; // mixing channel index
};

AudioSample* load_audio_sample(const char *path);
AudioSource* create_audio_source(AudioSample *sample);

void play_audio(AudioSource *audio_source,
		AudioPlayMode mode = AudioPlayMode_once);

void pause_audio(AudioSource *audio_source);
void resume_audio(AudioSource *audio_source);
void stop_audio(AudioSource *audio_source);
void set_audio_volume(AudioSource *audio_source, int volume); // volume 1 - 100
void set_master_volume(int volume); // volume 1 - 100
bool is_audio_played(AudioSource *audio_source);
bool is_audio_paused(AudioSource *audio_source);
int get_mixing_channel_count();

#endif
