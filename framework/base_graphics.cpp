/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#if defined _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "base_graphics.h"

void set_renderer_background_color(SDL_Renderer *renderer, SDL_Color color){
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
  SDL_RenderFillRect(renderer, NULL);
}

void draw_line(SDL_Renderer *renderer,
	       float x1,
	       float y1,
	       float x2,
	       float y2,
	       SDL_Color color){

  SDL_SetRenderDrawColor(renderer,
			 color.r,
			 color.g,
			 color.b,
			 color.a);

  SDL_RenderDrawLine(renderer,
		     x1, y1,
		     x2, y2);
}

void draw_line(SDL_Renderer *renderer,
	       v2 point1,
	       v2 point2,
	       SDL_Color color){

  SDL_SetRenderDrawColor(renderer,
			 color.r,
			 color.g,
			 color.b,
			 color.a);

  draw_line(renderer,
	    point1.x, point1.y,
	    point2.x, point2.y,
	    color);
}

void draw_line(SDL_Renderer *renderer,
	       Transform *transform1,
	       Transform *transform2,
	       SDL_Color color){

  SDL_SetRenderDrawColor(renderer,
			 color.r,
			 color.g,
			 color.b,
			 color.a);

  draw_line(renderer,
	    transform1->position,
	    transform2->position,
	    color);
}

void draw_rectangle(SDL_Renderer *renderer,
		    float width,
		    float height,
		    v2 position,
		    SDL_Color color,
		    DrawMode mode){

  v2i screen_position = get_screen_position(position.x, position.y);

  SDL_Rect rect = {
		   (int)(screen_position.x - ceil(width / 2)),
		   (int)(screen_position.y - ceil(height / 2)),
		   (int)width,
		   (int)height
  };

  SDL_SetRenderDrawColor(renderer,
			 color.r,
			 color.g,
			 color.b,
			 color.a);

  switch(mode){
  case DrawMode_fill:
  SDL_RenderFillRect(renderer, &rect);
    break;
  case DrawMode_draw: 
  SDL_RenderDrawRect(renderer, &rect);
    break;
  }

}

void draw_rectangle(SDL_Renderer *renderer,
		    Rectangle *rectangle,
		    SDL_Color color,
		    DrawMode mode){

  draw_rectangle(renderer,
		 rectangle->width,
		 rectangle->height,
		 rectangle->position,
		 color,
		 mode);
}


void draw_circle(SDL_Renderer *renderer,
		 float radius,
		 v2 position,
		 SDL_Color color,
		 DrawMode mode){

  v2i screen_position = {(int)position.x, (int)position.y};

  switch(mode){

  case DrawMode_fill:
    filledCircleRGBA(renderer,
		     screen_position.x,
		     screen_position.y,
		     radius,
		     color.r, color.g, color.b, color.a);
    break;

  case DrawMode_draw:
    circleRGBA(renderer,
	       screen_position.x,
	       screen_position.y,
	       radius,
	       color.r, color.g, color.b, color.a);
    break;
  }
}

void draw_circle(SDL_Renderer *renderer,
		 Circle *circle,
		 SDL_Color color,
		 DrawMode mode){

  draw_circle(renderer,
	      circle->radius,
	      circle->position,
	      color,
	      mode);
}

// texture
SDL_Texture* create_texture(SDL_Renderer *renderer,
			    SDL_Surface *surface,
                            int flag){

  SDL_Texture *texture = nullptr;
  texture = SDL_CreateTextureFromSurface(renderer, surface); 
  if(flag >= 0) SDL_FreeSurface(surface);
  return texture;
}

SDL_Texture* create_texture(SDL_Renderer *renderer,
			    int width,
			    int height){

  SDL_Texture *texture =
    SDL_CreateTexture(renderer,
		      SDL_PIXELFORMAT_RGBA8888,
		      SDL_TEXTUREACCESS_TARGET,
		      width,
		      height);

  return texture;
}

SDL_Surface* load_surface(const char *path){
  SDL_Surface *surface = IMG_Load(path);
  
  if(!surface){

    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		 "Unable load image: %s",
		 SDL_GetError()
		 );

    return nullptr;
  }

  return surface;
}

SDL_Surface* copy_surface(SDL_Surface* surface, SDL_Rect *sub_rect){
  int cp_width = 0;
  int cp_height = 0;
  
  if(!sub_rect){
    cp_width = surface->w;
    cp_height = surface->h;
  }
  else{
    cp_width = sub_rect->w;
    cp_height = sub_rect->h;
  }

  SDL_Surface *cp_surface =
    SDL_CreateRGBSurface(0,
			 cp_width,
			 cp_height,
			 32,
			 surface->format->Rmask,
			 surface->format->Gmask,
			 surface->format->Bmask,
			 surface->format->Amask);

  SDL_BlitSurface(surface, sub_rect, cp_surface, nullptr);
  return cp_surface;
}

SDL_Texture* load_texture(SDL_Renderer *renderer, const char *path){
  SDL_Surface *surface = load_surface(path);
  if(surface) return create_texture(renderer, surface);
  return nullptr;
}

void draw_texture(SDL_Renderer *renderer,
		  SDL_Texture *texture,
		  int width,
		  int height,
		  float pos_x,
		  float pos_y){

  SDL_Rect dst_rect = {0, 0, 0, 0};
  v2i screen_position = get_screen_position(pos_x, pos_y);
  dst_rect.x = screen_position.x - (width / 2);
  dst_rect.y = screen_position.y - (height / 2);
  dst_rect.w = width;
  dst_rect.h = height;

  SDL_RenderCopyEx(renderer,
		   texture,
		   nullptr,
		   &dst_rect,
		   0,
		   nullptr,
		   SDL_FLIP_NONE);
}

// sprite
Sprite* create_sprite(SDL_Texture *texture){
  Sprite *sprite = (Sprite*)malloc(sizeof(Sprite));
  sprite->texture = texture;
  sprite->is_visible = true;

  SDL_QueryTexture(texture,
		   nullptr,
		   nullptr,
		   &sprite->width,
		   &sprite->height);

  sprite->pivot = nullptr;

  return sprite;
}

void free_sprite(Sprite *sprite){
  if(sprite->pivot) free(sprite->pivot);
  SDL_DestroyTexture(sprite->texture);
  free(sprite);
}

void draw_sprite(SDL_Renderer *renderer,
		 Sprite *sprite,
		 float pos_x,
		 float pos_y,
		 SDL_Rect *sub_rect){

  SDL_Rect dst_rect = {0, 0, 0, 0};
  SDL_Point pivot = {0, 0};
  int width = 0;
  int height = 0;
  
  if(sub_rect){
    width = sub_rect->w;
    height = sub_rect->h;
  }
  else{
    width = sprite->width;
    height = sprite->height;
  }

  if(sprite->pivot){
    pivot.x = sprite->pivot->x;
    pivot.y = sprite->pivot->y;
  }
  else{
    pivot = (SDL_Point){width / 2, height / 2};
  }

  v2i screen_position = get_screen_position(pos_x, pos_y);
  dst_rect.x = screen_position.x - (width / 2);
  dst_rect.y = screen_position.y - (height / 2);
  dst_rect.w = width;
  dst_rect.h = height;

  if(sprite->is_visible){
    SDL_RenderCopyEx(renderer,
		     sprite->texture,
		     sub_rect,
		     &dst_rect,
		     0,
		     &pivot,
		     SDL_FLIP_NONE);
  }
}

void draw_sprite(SDL_Renderer *renderer,
		 Sprite *sprite,
		 Transform *transform,
		 SDL_Rect *sub_rect){

  SDL_Rect dst_rect = {0, 0, 0, 0};
  SDL_Point pivot = {0, 0};
  float pos_x = transform->position.x;
  float pos_y = transform->position.y;
  int scaled_width = 0;
  int scaled_height = 0;
  
  if(sub_rect){
    scaled_width = (float)sub_rect->w * transform->scale.x;
    scaled_height = (float)sub_rect->h * transform->scale.y;
  }
  else{
    scaled_width = (float)sprite->width * transform->scale.x;
    scaled_height = (float)sprite->height * transform->scale.y;
  }

  if(sprite->pivot){
    pivot.x = sprite->pivot->x;
    pivot.y = sprite->pivot->y;
  }
  else{
    pivot = (SDL_Point){scaled_width / 2, scaled_height / 2};
  }

  v2i screen_position = get_screen_position(pos_x, pos_y);
  dst_rect.x = screen_position.x - (scaled_width / 2);
  dst_rect.y = screen_position.y - (scaled_height / 2);
  dst_rect.w = scaled_width;
  dst_rect.h = scaled_height;

  SDL_RenderCopyEx(renderer,
		   sprite->texture,
		   sub_rect,
		   &dst_rect,
		   transform->rotation,
		   &pivot,
		   SDL_FLIP_NONE);
}

BASE_GRAPHICS
SDL_Rect get_sub_rect(int index,
		      int column,
		      int width,
		      int height){

  // (i * column) + j = (frame at index - 1)

  int i = index / column;
  int j = index - (i * column);
  SDL_Rect sub_rect;
  sub_rect.x = j * width;
  sub_rect.y = i * height;
  sub_rect.w = width;
  sub_rect.h = height;
  return sub_rect;
}

SpriteAnimator* create_sprite_animator(Sprite *sprite, v2i frame_size){

  SpriteAnimator *sprite_animator =
    (SpriteAnimator*)malloc(sizeof(SpriteAnimator));

  sprite_animator->sprite = sprite;
  sprite_animator->frame_width = frame_size.x;
  sprite_animator->frame_height = frame_size.y;
  sprite_animator->sprite_column = sprite->width / frame_size.x;
  sprite_animator->sprite_row = sprite->height / frame_size.y;
  sprite_animator->frame_index = 0;
  sprite_animator->last_frame_list = nullptr;
  sprite_animator->ticks = 0;

  sprite_animator->sub_rect =
    get_sub_rect(sprite_animator->frame_index,
		 sprite_animator->sprite_column,
		 sprite_animator->frame_width,
		 sprite_animator->frame_height);


  return sprite_animator;
}

void run_sprite_animator(SpriteAnimator *sprite_animator,
			 int *frame_list,
			 int frame_count,
			 int fps,
			 AnimPlay anim_play){

  double ticks_next_frame = 1000 / fps;

  if(sprite_animator->last_frame_list == nullptr){
    sprite_animator->last_frame_list = frame_list;
  }
  else{
    
    if(sprite_animator->last_frame_list != frame_list){
      sprite_animator->last_frame_list = frame_list;
      sprite_animator->frame_index = 0;
      sprite_animator->ticks = 0;
    }

  }

  if(sprite_animator->ticks == 0)
    sprite_animator->ticks = SDL_GetTicks();

  int index =
    sprite_animator->last_frame_list[sprite_animator->frame_index] - 1;

  sprite_animator->sub_rect =
    get_sub_rect(index,
		 sprite_animator->sprite_column,
		 sprite_animator->frame_width,
		 sprite_animator->frame_height);

  if(SDL_GetTicks() >= sprite_animator->ticks + ticks_next_frame){

    if(sprite_animator->frame_index < frame_count - 1){
      ++sprite_animator->frame_index;
    }
    else{

      if(anim_play == AnimPlay_loop)
	sprite_animator->frame_index = 0;

    }

    sprite_animator->ticks = 0;
  }

}

void draw_sprite_animator(SDL_Renderer *renderer,
			  SpriteAnimator *sprite_animator,
			  float pos_x,
			  float pos_y){

  draw_sprite(renderer,
	      sprite_animator->sprite,
	      pos_x,
	      pos_y,
	      &sprite_animator->sub_rect);
  
}

void draw_sprite_animator(SDL_Renderer *renderer,
			  SpriteAnimator *sprite_animator,
			  Transform *transform){

  draw_sprite(renderer,
	      sprite_animator->sprite,
	      transform,
	      &sprite_animator->sub_rect);
  
}

// tiledmap
Tiledmap* create_tiledmap(SDL_Renderer *renderer,
			  const char *path,
			  char *root_path,
			  Tileset **tileset_list,
			  int tileset_count){

  Tiledmap *tiledmap = (Tiledmap*)malloc(sizeof(Tiledmap));
  LuaState *L = create_lua_state();
  tiledmap->tileset_count = 0;
  tiledmap->layer_count = 0;

  if( load_lua_file(L, path) ){

    if(lua_istable(L, -1)){

      tiledmap->column = get_lua_int(L, "width");
      tiledmap->row = get_lua_int(L, "height");
      tiledmap->tile_width = get_lua_int(L, "tilewidth");
      tiledmap->tile_height = get_lua_int(L, "tileheight");
      tiledmap->orientation = get_lua_cstr(L, "orientation");
      tiledmap->tileset_count = get_lua_length(L, "tilesets");
      tiledmap->layer_count = get_lua_length(L, "layers");

      if(tileset_list){
	tiledmap->tileset_list = tileset_list;
	tiledmap->tileset_count = tileset_count; 
      }
      else{

	tiledmap->tileset_list =
	  (Tileset**)malloc(tiledmap->tileset_count * sizeof(Tileset));

	// get tilesets data
	lua_pushstring(L, "tilesets");
	lua_gettable(L, -2);

	int tileset_index = 0;
	lua_pushnil(L);
	while(lua_next(L, -2)){

	  tiledmap->tileset_list[tileset_index] =
	    (Tileset*)malloc(sizeof(Tileset));

	  tiledmap->tileset_list[tileset_index]->tileset_index =
	    tileset_index;

	  tiledmap->tileset_list[tileset_index]->tile_width =
	    get_lua_int(L, "tilewidth");

	  tiledmap->tileset_list[tileset_index]->tile_height =
	    get_lua_int(L, "tileheight");

	  tiledmap->tileset_list[tileset_index]->first_grid =
	    get_lua_int(L, "firstgid");

	  tiledmap->tileset_list[tileset_index]->tile_count =
	    get_lua_int(L, "tilecount");

	  std::string path = "";

	  if(root_path){
	    path = root_path;
	    path += get_lua_string(L, "image");
	  }
	  else{
	    path = get_lua_string(L, "image");
	  }

	  SDL_Texture *texture = load_texture(renderer, path.c_str());

	  tiledmap->tileset_list[tileset_index]->sprite =
	    create_sprite(texture);

	  ++tileset_index;
	  lua_pop(L, 1);
	}

	lua_pop(L, 1);

      } // else if !tileset_list

      tiledmap->layer_list =
	(TiledmapLayer**)malloc(tiledmap->layer_count * sizeof(TiledmapLayer));

      // get layers data
      lua_pushstring(L, "layers");
      lua_gettable(L, -2);

      int layer_index = 0;
      lua_pushnil(L);
      while(lua_next(L, -2)){

	tiledmap->layer_list[layer_index] =
	  (TiledmapLayer*)malloc(sizeof(TiledmapLayer));

	tiledmap->layer_list[layer_index]->name =
	  get_lua_cstr(L, "name");

	tiledmap->layer_list[layer_index]->map_size =
	  get_lua_length(L, "data");

	// map_arr[i][j]
	// size i
	tiledmap->layer_list[layer_index]->map_arr =
	  (int**)malloc( tiledmap->row * sizeof(int*) );

	// size j
	for(int i = 0; i < tiledmap->row; ++i){
	  tiledmap->layer_list[layer_index]->map_arr[i] =
	    (int*)malloc( tiledmap->column * sizeof(int) );
	}


	// get layer data
  	lua_pushstring(L, "data");
  	lua_gettable(L, -2);

	int count = 0;
  	lua_pushnil(L);

  	while(lua_next(L, -2)){
	  int i = count / tiledmap->column;
	  int j = count - (i * tiledmap->column);

	  tiledmap->layer_list[layer_index]->map_arr[i][j] =
	    lua_tointeger(L, -1);

  	  ++count;
  	  lua_pop(L, 1);
  	}

	lua_pop(L, 2);
	++layer_index;
      }

      lua_pop(L, 1);

    }

  }

  lua_close(L);

  tiledmap->max_draw_size = (v2i){tiledmap->column, tiledmap->row};
  return tiledmap;

}

void print_tiledmap_info(Tiledmap *tiledmap){
  cout<<"map column:"<<tiledmap->column<<endl;
  cout<<"map row:"<<tiledmap->row<<endl;
  cout<<"map orientation:"<<tiledmap->orientation<<endl;
  cout<<"tile width:"<<tiledmap->tile_width<<endl;
  cout<<"tile height:"<<tiledmap->tile_height<<endl;
  cout<<"tileset count:"<<tiledmap->tileset_count<<endl;
  cout<<"layer count:"<<tiledmap->layer_count<<endl;
  cout<<"max draw size(column, row):"<<tiledmap->max_draw_size<<endl;

  for(int tileset_index = 0; tileset_index < tiledmap->tileset_count; ++tileset_index){
    cout<<"tileset index:"<<tiledmap->tileset_list[tileset_index]->tileset_index<<endl;
    cout<<"tileset tile width:"<<tiledmap->tileset_list[tileset_index]->tile_width<<endl;
    cout<<"tileset tile height:"<<tiledmap->tileset_list[tileset_index]->tile_height<<endl;
    cout<<"tileset first grid:"<<tiledmap->tileset_list[tileset_index]->first_grid<<endl;
    cout<<"tileset tile count:"<<tiledmap->tileset_list[tileset_index]->tile_count<<endl;
  }

  for(int layer_index = 0; layer_index < tiledmap->layer_count; ++layer_index){
    cout<<"layer name:"<<tiledmap->layer_list[layer_index]->name<<endl;
    cout<<"layer map size:"<<tiledmap->layer_list[layer_index]->map_size<<endl;

    for(int i = 0; i < tiledmap->row; ++i){
      for(int j = 0; j < tiledmap->column; ++j){
	cout << tiledmap->layer_list[layer_index]->map_arr[i][j] <<", ";
      }
      cout<<endl;
    }

    cout<<endl;
  }
}

BASE_GRAPHICS
int get_tileset_index(int tile,
		      Tileset **tileset,
		      int tileset_count){

  for(int i = 0; i < tileset_count; ++i){

    bool min = tile >= tileset[i]->first_grid;
    bool max = tile <= (tileset[i]->first_grid - 1) +
      tileset[i]->tile_count; 

    if(min && max){
      return i;
    }

  } // for

  return -1;
}

v2 get_tiledmap_grid_position(Tiledmap *tiledmap,
		     int column,
		     int row,
		     v2 position){
  
  v2 grid_position = v2_0;
  float x = column * tiledmap->tile_width;
  float y = row * tiledmap->tile_height;
  grid_position.x = position.x + x;
  grid_position.y = position.y + y;
  return grid_position;
}

v2i get_tiledmap_grid(Tiledmap *tiledmap,
	     v2 position,
	     v2 tiledmap_position){

  v2i grid = v2i_0;
  int column = ( ( position.x + (tiledmap->tile_width / 2) ) - tiledmap_position.x) / tiledmap->tile_width;
  int row = ( ( position.y + (tiledmap->tile_height / 2) ) - tiledmap_position.y) / tiledmap->tile_height;
  grid.x = column;
  grid.y = row;
  return grid;
}

bool is_tiledmap_tiled(Tiledmap *tiledmap,
	      int layer_index,
	      int column,
	      int row){

  if(column > 0 && column < tiledmap->column &&
     row > 0 && row < tiledmap->row){

    if(tiledmap->layer_list[layer_index]->map_arr[row][column] > 0){
      return true;
    }

  }

  return false;
}

BASE_GRAPHICS
void draw_tiledmap_per_layer(SDL_Renderer *renderer,
			     Tiledmap *tiledmap,
			     float pos_x,
			     float pos_y,
			     int layer_index){

 for(int i = 0; i < tiledmap->max_draw_size.y; ++i){
    for(int j = 0; j < tiledmap->max_draw_size.x; ++j){

      int tile =
	tiledmap->layer_list[layer_index]->map_arr[i][j];

      float draw_x = pos_x + (j * (float)tiledmap->tile_width);
      float draw_y = pos_y + (i * (float)tiledmap->tile_height);

      if(tile > 0){

	int tileset_index =
	  get_tileset_index(tile,
			    tiledmap->tileset_list,
			    tiledmap->tileset_count);

	int index =
	  tile - tiledmap->tileset_list[tileset_index]->first_grid;

	SDL_Rect sub_rect = get_sub_rect(index,
					 tiledmap->column,
					 tiledmap->tile_width,
					 tiledmap->tile_height);

	draw_sprite(renderer,
		    tiledmap->tileset_list[tileset_index]->sprite,
		    draw_x,
		    draw_y,
		    &sub_rect
		    );
      }

    } // for j
  } // for i

}

void draw_tiledmap(SDL_Renderer *renderer,
		   Tiledmap *tiledmap,
		   float pos_x,
		   float pos_y,
		   int layer_index){

  if(layer_index < 0){ // draw all layer

    for(int i = 0; i < tiledmap->layer_count; ++i){

      draw_tiledmap_per_layer(renderer,
			      tiledmap,
			      pos_x,
			      pos_y,
			      i);
    } // for

  }
  else{

    draw_tiledmap_per_layer(renderer,
			    tiledmap,
			    pos_x,
			    pos_y,
			    layer_index);
  }

}

// texture atlas
BASE_GRAPHICS
bool is_white_space(char *line, int index){
  if(isspace(line[index]) != 0){
    return true;
  }
  return false;
}

BASE_GRAPHICS
bool is_contain(char *line, char c){
  int len = strlen(line);
  for(int i = 0; i < len; ++i){
    if(line[i] == c){
      return true;
    }
  }
  return false;
}

TextureAtlas* load_texture_atlas(const char *path){
  FILE *file = fopen(path, "r");
  size_t line_len = 70;
  fun::string line = create_string(line_len);

  if(!file){
    cout << "Failed to open file:" << path << endl;
    exit(1);
  }

  TextureAtlas *texture_atlas =
    (TextureAtlas*)malloc(sizeof(TextureAtlas));

  texture_atlas->pack_count = 0;

  Surface *texture_atlas_surface = nullptr;
  TextureAtlasPack *pack = nullptr;
  SDL_Rect sub_rect;
  bool is_rotate = false;
  while(fgets((char*)line.c_str, line_len, file) != 0){
    // cout << line;

    if(!is_contain((char*)line.c_str, ':') &&
       !is_contain((char*)line.c_str, '.') &&
       !is_white_space((char*)line.c_str, 0)){ // section

      // reset rotate flag
      is_rotate = false;

      // remove '\n' at end of string
      line.c_str = strtok((char*)line.c_str, "\n");

      // create texture atlas pack
      pack = (TextureAtlasPack*)malloc(sizeof(TextureAtlasPack));
      pack->name = (char*)malloc(line.length());
      strcpy((char*)pack->name, line.c_str);

      ++texture_atlas->pack_count;

      if(texture_atlas->pack_count == 1){
	texture_atlas->pack_list =
	  (TextureAtlasPack**)malloc(texture_atlas->pack_count * sizeof(TextureAtlasPack*));
      }
      else{
	size_t realloc_size = texture_atlas->pack_count;

	TextureAtlasPack** realloc_pack =
	  (TextureAtlasPack**)realloc(texture_atlas->pack_list, realloc_size * sizeof(TextureAtlasPack*));

	if(realloc_pack){
	  texture_atlas->pack_list = realloc_pack;
	}
	else{
	  free(texture_atlas->pack_list);
	  exit(1);
	}
      }

    }
    else if(!is_contain((char*)line.c_str, ':') &&
	    is_contain((char*)line.c_str, '.') &&
	    !is_white_space((char*)line.c_str, 0)){ // image path

      // remove '\n' at end of string
      line.c_str = strtok((char*)line.c_str, "\n");

      // load surface
      texture_atlas_surface = load_surface(line.c_str);
    }
    else if(is_contain((char*)line.c_str, ':') &&
	    !is_white_space((char*)line.c_str, 0)){ // value pair
    }
    else if(is_contain((char*)line.c_str, ':') &&
	    is_white_space((char*)line.c_str, 0)){ // section value pair

      char *str = strtok((char*)line.c_str, " \n:,");
      char **token_list = (char**)malloc(3 * sizeof(char*)); 
      int token_count = 0;

      while(str!= nullptr){
	token_list[token_count] = str;
	++token_count;
	str = strtok(nullptr, " \n:,");
      } // while str != nullptr

      if( fun::string::equal(token_list[0], "rotate") ){
	if( fun::string::equal(token_list[1], "true") ){
	  is_rotate = true;
	}
	else{
	  is_rotate = false;
	}
      }
      else if( fun::string::equal(token_list[0], "xy") ){
	sub_rect.x = atoi(token_list[1]);
	sub_rect.y = atoi(token_list[2]);
      }
      else if( fun::string::equal(token_list[0], "size") ){
	if(is_rotate){
	  sub_rect.w = atoi(token_list[2]);
	  sub_rect.h = atoi(token_list[1]);
	}
	else{
	  sub_rect.w = atoi(token_list[1]);
	  sub_rect.h = atoi(token_list[2]);
	}
      }
      else if( fun::string::equal(token_list[0], "orig") ){
      }
      else if( fun::string::equal(token_list[0], "offset") ){
      }
      else if( fun::string::equal(token_list[0], "index") ){
	int index = atoi(token_list[1]);

	if(index > -1){
	  fun::string::append((char*)pack->name, "_");
	  fun::string::append((char*)pack->name, token_list[1]);
	} // index > -1
      }

      pack->surface = copy_surface(texture_atlas_surface, &sub_rect);
      texture_atlas->pack_list[texture_atlas->pack_count - 1] = pack;

      str = nullptr;
      free(token_list);
      token_list = nullptr;
    } // section value pair
  } // while fgets

  line.clear();
  free((char*) line.c_str);
  line.c_str = nullptr;

  fclose(file);
  SDL_FreeSurface(texture_atlas_surface);

  return texture_atlas;
}

SDL_Surface* copy_texture_atlas_surface(TextureAtlas *texture_atlas, const char *name){
  for(int i = 0; i < texture_atlas->pack_count; ++i){
    if( fun::string::equal(texture_atlas->pack_list[i]->name,  name) ){
      return copy_surface(texture_atlas->pack_list[i]->surface);
    } // if
  } // for
  return nullptr;
}

SDL_Surface* copy_texture_atlas_surface(TextureAtlas *texture_atlas, int index){
  return copy_surface(texture_atlas->pack_list[index]->surface);
}

void free_texture_atlas(TextureAtlas* texture_atlas){

  for(int i = 0; i < texture_atlas->pack_count; ++i){

    if(texture_atlas->pack_list[i]->surface)
      SDL_FreeSurface(texture_atlas->pack_list[i]->surface);

    free((char*)texture_atlas->pack_list[i]->name);
    texture_atlas->pack_list[i]->name = nullptr;
    free(texture_atlas->pack_list[i]);
  }

  free(texture_atlas->pack_list);
  free(texture_atlas);
}

// TTF 
TTF_Font* load_font(const char *path, int size){
  TTF_Font *font = nullptr;
  font = TTF_OpenFont(path, size);

  if(!font){
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
		 "Failed load font: %s",
		 SDL_GetError()
		 );
  }
  else{
    return font;
  }

  return nullptr;
}

void draw_text(SDL_Renderer *renderer,
	       TTF_Font *font,
	       const char *text,
	       float pos_x,
	       float pos_y,
	       SDL_Color fg_color,
	       SDL_Color bg_color){

  v2i screen_position = get_screen_position(pos_x, pos_y);
  SDL_Rect rect = {screen_position.x,
		   screen_position.y,
		   0, 0};

  TTF_SizeText(font, text, &rect.w, &rect.h);

  SDL_Surface *surface = nullptr;

  if(bg_color.a == 0){ // check if bg color is transparent
    surface = TTF_RenderText_Blended(font, text, fg_color);
  }
  else{
    surface = TTF_RenderText_Shaded(font, text, fg_color, bg_color);
  }

  SDL_Texture *texture = create_texture(renderer, surface);
  SDL_RenderCopy(renderer, texture, nullptr, &rect);
  SDL_DestroyTexture(texture);
}

// content view
ContentView* create_content_view(SDL_Renderer *renderer,
				 float pos_x,
				 float pos_y,
				 int width,
				 int height){

  ContentView *content_view = (ContentView*)malloc(sizeof(ContentView));

  content_view->texture = create_texture(renderer, width, height);
  content_view->pos_x = pos_x;
  content_view->pos_y = pos_y;
  content_view->width = width;
  content_view->height = height;
  return content_view;

}

void start_target_content_view(SDL_Renderer *renderer, ContentView *content_view){
  SDL_SetRenderTarget(renderer, content_view->texture);
  SDL_SetTextureBlendMode(content_view->texture, SDL_BLENDMODE_BLEND);
}

void end_target_content_view(SDL_Renderer *renderer, ContentView *content_view){
  SDL_SetRenderTarget(renderer, NULL);
  v2i screen_position = get_screen_position(content_view->pos_x, content_view->pos_y);

  SDL_Rect dst_rect = {screen_position.x,
		       screen_position.y,
		       content_view->width,
		       content_view->height};

  SDL_RenderCopy(renderer, content_view->texture, NULL, &dst_rect);
}

// texture mod
void surface_mod_pixel_color(SDL_Surface *surface, SDL_Color color){
  Uint32 *pixels = (Uint32*)surface->pixels;

  Uint32 color_u32 =
    SDL_MapRGBA(surface->format,
		color.r,
		color.g,
		color.b,
		color.a);

  SDL_LockSurface(surface);
  
  for(int i = 0; i < surface->h; ++i){
    for(int j = 0; j < surface->w; ++j){
      Uint32 pixel_index = i * (surface->pitch / 4) + j; // 4 is sizeof what? RGBA? how about RGB then?
      if(pixels[pixel_index] != 0){
	pixels[pixel_index] = color_u32;
      } // if pixels[pixel_index] != 0
    } // for j
  } // for i
  
  SDL_UnlockSurface(surface);
}

void texture_mod_color(SDL_Texture *texture, SDL_Color color){
  SDL_SetTextureColorMod(texture, color.r, color.g, color.b);
}

void texture_mod_alpha(SDL_Texture *texture, Uint8 alpha){
  SDL_SetTextureAlphaMod(texture, alpha);
}
