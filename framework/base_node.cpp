/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_node.h"

Node* create_node(const char *_name, Transform *transform){

  Node *node = (Node*)malloc(sizeof(Node));
  create_string(&node->name, UNNAMED_NODE);
  create_string(&node->tag, UNTAGGED_NODE);
  node->component = create_vector<void*>();
  node->child = create_vector<Node*>();
  node->parent = nullptr;

  if(strcmp(_name, UNNAMED_NODE) != 0)
    node->name = _name; 
  
  if(!transform){
    node->transform = create_transform();
  }
  else{
    node->transform = transform;
  }
  
  return node;
}

void set_parent_node(Node *node, Node *_parent){
  if(_parent != node){

    fun::vector<Node*> *node_parent_child = nullptr;

    if(node->parent != nullptr){
      node_parent_child = &node->parent->child;

      if(node->parent != _parent){
	int index = node_parent_child->find(node);

	if(index > -1)
	  node_parent_child->erase(index);

      } // != _parent;
    } // != nullptr

    node->parent = _parent;

    if(_parent){
      _parent->child.push_back(node);

      v2 rel_position = node->transform->position - _parent->transform->position;
      node->transform->rel_position = rel_position; 

      node->transform->rel_rotation = get_azimuth(node->transform->position,
						  _parent->transform->position);
    }

  }

}

void add_child_node(Node *parent, Node* child){
  set_parent_node(child, parent);
}

void remove_node_child(Node *parent, Node *child){
  child->transform->rel_position = v2_0;
  child->transform->rel_rotation = 0.0f;
  int index = parent->child.find(child);
  parent->child.erase(index);
  child->transform->rel_position = v2_0;
  child->parent = nullptr;
}

void remove_node_child(Node *parent, int index){
  parent->child[index]->transform->rel_position = v2_0;
  parent->child[index]->transform->rel_rotation = 0.0f;
  parent->child[index]->transform->rel_position = v2_0;
  parent->child[index]->parent = nullptr;
  parent->child.erase(index);
}

void remove_node_parent(Node *node){
  remove_node_child(node->parent, node);
}

v2 to_node_rel_position(v2 point, Node *node){
  return (v2){point.x - node->transform->position.x, point.y - node->transform->position.y};
}

void set_node_position(Node *node, v2 position){
  v2 elapsed_position = position - node->transform->position;
  node->transform->position = position;

  for(int i = 0; i < node->child.size; ++i){
    Node *child = node->child[i];
    child->transform->position += elapsed_position;

    v2 rel_position = node->transform->position - child->transform->position;
    child->transform->rel_position = rel_position; 
  }
}
void set_node_rotation(Node *node, float rotation){

  float elapsed_rotation = rotation - node->transform->rotation;
  node->transform->rotation += rotation;

  for(int i = 0; i < node->child.size; ++i){
    Node *child = node->child[i];

    child->transform->rotation = node->transform->rotation;

    v2 rotation_position =
      transform_rotation(child->transform->position,
			 node->transform->position, 
			 rotation);

    child->transform->position = rotation_position; 
  }
}

void free_node(Node *node){
  // free transform
  free(node->transform);

  // clear child vector element
  if(node->child.size > 0)
    node->child.clear();

  // clear component vector and free memory allocation
  if(node->component.size > 0)
    node->component.clear_and_free_alloc();

  free(node);
  node = nullptr;
}
