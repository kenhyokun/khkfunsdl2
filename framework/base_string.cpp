/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_string.h"

// string static function

char* fun::string::alloc_string(const char* str){
  size_t len = strlen(str);
  char *alloc = (char*)malloc(sizeof(char) * len);
  strcpy(alloc, str);
  return alloc;
}

void fun::string::append(char *dst_str, const char *cat_str ){
   size_t len = strlen(dst_str) + strlen(cat_str);
   dst_str = (char*) realloc((char*)dst_str, len);
   strcat(dst_str, cat_str);
}

bool fun::string::equal(const char *str1, const char *str2){
  if(strcmp(str1, str2) == 0){
    return true;
  }
  return false;
}

size_t fun::string::find(const char*str, char _char, size_t pos){
  size_t len = strlen(str);
  for(int i = pos; i < len; ++i){
    if(str[i] ==  _char){
       return i;
    }
  }
  return STRING_NPOS;
}

size_t fun::string::find(const char*str1, const char *str2, size_t pos){
  size_t str1_len = strlen(str1);
  size_t str2_len = strlen(str2);

  char *temp_str = fun::string::substr(str1, pos, str2_len);
  size_t temp_str_len = strlen(temp_str);

  if(pos + temp_str_len < str1_len){
    if(fun::string::equal(temp_str, str2)){
      free(temp_str);
      return pos;
    }
    else{
      free(temp_str);
      ++pos;
      return fun::string::find(str1, str2, pos);
    }
  }

  free(temp_str);
  return STRING_NPOS;
}

char* fun::string::substr(const char* str, size_t pos, size_t len){
  char *sub = (char*)malloc(sizeof(char) * len);
  strncpy(sub, str + pos, len);
  sub[len] = '\0';
  return sub;
}

// string public function
size_t fun::string::length(){
  return strlen(c_str);
}

char fun::string::at(int index){
  return c_str[index];
}

void fun::string::operator= (const char *_c_str){
  size_t len = strlen(_c_str);
  c_str = (char*) realloc((char*)c_str, len);
  strcpy((char*)c_str, _c_str);
}

void fun::string::operator= (const std::string &_string){
  size_t len = strlen(_string.c_str());
  c_str = (char*) realloc((char*)c_str, len);
  strcpy((char*)c_str, _string.c_str());
}

void fun::string::operator= (const fun::string &_string){
  size_t len = strlen(_string.c_str);
  c_str = (char*) realloc((char*)c_str, len);
  strcpy((char*)c_str, _string.c_str);
}

void fun::string::operator+= (const char *_c_str){
  size_t len = strlen(c_str) + strlen(_c_str);
  c_str = (char*) realloc((char*)c_str, len);
  strcat((char*)c_str, _c_str);
}

void fun::string::operator+= (const fun::string &_string){
  size_t len = strlen(c_str) + strlen(_string.c_str);
  c_str = (char*) realloc((char*)c_str, len);
  strcat((char*)c_str, _string.c_str);
}

void fun::string::operator+= (const std::string &_string){
  size_t len = strlen(c_str) + strlen(_string.c_str());
  c_str = (char*) realloc((char*)c_str, len);
  strcat((char*)c_str, _string.c_str());
}

bool fun::string::operator== (const char *_c_str){
  if(strcmp(c_str, _c_str) == 0){
    return true;
  }
  return false;
}

bool fun::string::operator== (const fun::string &_string){
  if(strcmp(c_str, _string.c_str) == 0){
    return true;
  }
  return false;
}

bool fun::string::operator== (const std::string &_string){
  if(strcmp(c_str, _string.c_str()) == 0){
    return true;
  }
  return false;
}

bool fun::string::operator!= (const char *_c_str){
  if(strcmp(c_str, _c_str) != 0){
    return true;
  }
  return false;
}

bool fun::string::operator!= (const fun::string &_string){
  if(strcmp(c_str, _string.c_str) != 0){
    return true;
  }
  return false;
}

bool fun::string::operator!= (const std::string &_string){
  if(strcmp(c_str, _string.c_str()) != 0){
    return true;
  }
  return false;
}

char fun::string::operator[] (const int &index){
  return c_str[index];
}

size_t fun::string::find(char _char, size_t pos){
  return fun::string::find(c_str, _char, pos);
}

size_t fun::string::find(const char *str, size_t pos){
  return fun::string::find(c_str, str, pos);
}

char* fun::string::substr(size_t pos, size_t len){
  return fun::string::substr(c_str, pos, len);
}

void fun::string::clear(){
  c_str = (char*) realloc((char*)c_str, 0);
  strcpy((char*)c_str, "");
}

void create_string(fun::string *fun_string, const char *str){
  fun_string->c_str = (char*)malloc(strlen(str));
  strcpy((char*)fun_string->c_str, str);
}

fun::string create_string(const char *str){
  fun::string fun_string;
  fun_string.c_str = (char*)malloc(strlen(str));
  strcpy((char*)fun_string.c_str, str);
  return fun_string;
}

fun::string create_string(size_t size){
  fun::string fun_string;
  fun_string.c_str = (char*)malloc(size);
  return fun_string;
}

