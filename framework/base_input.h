/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_INPUT
#define BASE_INPUT

#include<iostream>
#include "base_sdl2_include.h"
#include "base_vector.h"

/*
  keyboard input
*/
struct KeyButton{
  const char *action_name;
  SDL_Keycode key_code;
  bool is_pressed;
  void(*key_down_callback)();
  void(*key_up_callback)();
};

KeyButton* create_key_button(const char *action_name, SDL_Keycode key_code);
void on_key_down(SDL_Event event, KeyButton *key_button);
void on_key_up(SDL_Event event, KeyButton *key_button);

/*
  mouse input
*/
struct MouseButton{
  Uint8 button_state;
  bool is_pressed;
  void(*button_down_callback)();
  void(*button_up_callback)();
};

struct MouseInput{
  fun::vector<MouseButton*> mouse_button_list;
  void(*on_mouse_motion)();
};

void on_mouse_button_down(SDL_Event event, MouseButton *mouse_button);
void on_mouse_button_up(SDL_Event event, MouseButton *mouse_button);
MouseButton* create_mouse_button(Uint8 button_state);

#endif
