/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_app.h"

SDL_Window *window = nullptr;
SDL_Renderer *renderer = nullptr;
int window_width;
int window_height;
int logical_screen_width;
int logical_screen_height;
bool is_running;
int ticks;
int frames;
long start;
long last_timer;
SDL_Event event;
MouseInput *mouse_input;
v2i mouse_position;
fun::vector<KeyButton*> key_button_list;


void create_app(int _window_width,
		int _window_height,
		const char *title,
		bool is_resizable,
		const char *scale_filter){

  mouse_input = (MouseInput*)malloc(sizeof(MouseInput));
  mouse_input->mouse_button_list = create_vector<MouseButton*>();
  mouse_input->on_mouse_motion = nullptr;

  mouse_position = v2i_0;
  is_running = true;

  window_width = _window_width;
  window_height = _window_height;
  logical_screen_width = window_width;
  logical_screen_height = window_height;

  SDL_Init(SDL_INIT_EVERYTHING);
  Uint32 window_flag;

  key_button_list = create_vector<KeyButton*>();

  if(is_resizable){
    window_flag = SDL_WINDOW_RESIZABLE;
  }
  else{
    window_flag = SDL_WINDOW_SHOWN;
  }

  window = SDL_CreateWindow(title,
			    SDL_WINDOWPOS_CENTERED,
			    SDL_WINDOWPOS_CENTERED,
			    window_width,
			    window_height,
			    window_flag);

  renderer = SDL_CreateRenderer(window, -1, 0);

  SDL_SetWindowMinimumSize(window, window_width, window_height);

  SDL_RenderSetLogicalSize(renderer,
			   logical_screen_width,
			   logical_screen_height);

  Viewport_Init(logical_screen_width, logical_screen_height);

  TTF_Init();

  int audio_format_flag =
    MIX_INIT_FLAC |
    MIX_INIT_MOD |
    MIX_INIT_MID |
    MIX_INIT_MP3 |
    MIX_INIT_OGG;

  Mix_Init(audio_format_flag);
  Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);

  // Scale filter
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, scale_filter);

}

void add_key_button(KeyButton *key_button){
  key_button_list.push_back(key_button);
}

void add_mouse_button(MouseButton *mouse_button){
  mouse_input->mouse_button_list.push_back(mouse_button);
}

void run_app(void (*on_update)(void),
             void (*on_draw)(void),
	     void (*on_draw_gui)(void),
             int target_fps){

  while(is_running){

    while(SDL_PollEvent(&event)){
      switch(event.type){
 
      case SDL_QUIT:
	is_running = false;
	break;
      case SDL_WINDOWEVENT:
 
	switch(event.window.event){
	case SDL_WINDOWEVENT_RESIZED:
	  SDL_GetWindowSize(window,
			    &window_width,
			    &window_height);
	  break;
	}
 
	break;		

      case SDL_KEYDOWN:
	for(int i = 0; i < key_button_list.size; ++i){
	  on_key_down(event, key_button_list.at(i));
	}
	break;	

      case SDL_KEYUP:
	for(int i = 0; i < key_button_list.size; ++i){
	  on_key_up(event, key_button_list.at(i));
	}
	break;

      case SDL_MOUSEMOTION:
	mouse_position = (v2i){event.motion.x, event. motion.y};

	if(mouse_input->on_mouse_motion) mouse_input->on_mouse_motion();

	break;

      case SDL_MOUSEBUTTONDOWN:

	for(int i = 0; i < mouse_input->mouse_button_list.size; ++i){
	  on_mouse_button_down(event, mouse_input->mouse_button_list.at(i));
	}

	break;

      case SDL_MOUSEBUTTONUP:

	for(int i = 0; i < mouse_input->mouse_button_list.size; ++i){
	   on_mouse_button_up(event, mouse_input->mouse_button_list.at(i));
	}

	break;

      } // switch
    } // sdl_pollevent

    // notch fps counter meet cplusplus guy fps counter
    // lets try this and see what will happens...

    float fps_tick = 1000.0f / (float)target_fps; 
    bool should_render = false;
    double unprocessed = 0;
    double ns_per_ticks = 1000000000.0 / (float)target_fps;

    if(start == 0){
      start = SDL_GetTicks();
      unprocessed += 1000000.0 / ns_per_ticks;
    }

    if(SDL_GetTicks() >= start + (fps_tick - unprocessed) ){
      ++ticks;

      (*on_update)();

      should_render = true;
    }

    if(should_render){
      ++frames;
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // letter box color
      SDL_RenderClear(renderer);

      (*on_draw)();

      (*on_draw_gui)();

      SDL_RenderPresent(renderer);
      start = 0;
      should_render=false;
    }
		
    SDL_Delay(unprocessed);

    if(SDL_GetTicks() - last_timer > 1000){
      last_timer += 1000;
      cout<<"frames:"<<frames<<", ticks:"<<ticks<<endl;
      frames = 0;
      ticks = 0;
    }

  }// while is_running
}

v2i get_mouse_position(){
  return mouse_position;
}

v2 get_mouse_position_f(){
  return (v2){(float)mouse_position.x, (float)mouse_position.y};
}

void clear_app(){
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);

  while(Mix_Init(0)){
    Mix_Quit();
  }

  Mix_CloseAudio();
}
