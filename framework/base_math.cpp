/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_math.h"

v2 v2::operator+ (const v2 &_v2){
  v2 result;
  result.x = x + _v2.x;
  result.y = y + _v2.y;
  return result;
}

void v2::operator+= (const v2 &_v2){
  x += _v2.x;
  y += _v2.y;
}

v2 v2::operator- (const v2 &_v2){
  v2 result;
  result.x = x - _v2.x;
  result.y = y - _v2.y;
  return result;
}

v2 v2::operator- (){
  v2 result;
  result.x = -x;
  result.y = -y;
  return result;
}

void v2::operator-= (const v2 &_v2){
  x -= _v2.x;
  y -= _v2.y;
}

bool v2::operator== (const v2 &_v2){
  bool is_equal_x = (x == _v2.x);
  bool is_equal_y = (y == _v2.y);

  if(is_equal_x && is_equal_y){
    return true;
  }

  return false;
}

bool v2::operator!= (const v2 &_v2){
  bool is_equal_x = (x == _v2.x);
  bool is_equal_y = (y == _v2.y);

  if(is_equal_x && is_equal_y){
    return false;
  }
  
  return true;
}

v2 v2::operator* (const v2 &_v2){
  v2 result;
  result.x = x * _v2.x;
  result.y = y * _v2.y;
  return result;
}

v2 v2::operator* (const float &val){
  v2 result;
  result.x = x * val;
  result.y = y * val;
  return result;
}

void v2::operator*= (const float &val){
  x *= val;
  y *= val;
}

std::ostream &operator<<(std::ostream &output, const v2 &_v2){
  output<< "(" << _v2.x << ", " << _v2.y << ")";
  return output;
}

v2i v2i::operator+ (const v2i &_v2i){
  v2i result;
  result.x = x + _v2i.x;
  result.y = y + _v2i.y;
  return result;
}

void v2i::operator+= (const v2i &_v2i){
  x += _v2i.x;
  y += _v2i.y;
}

v2i v2i::operator- (const v2i &_v2i){
  v2i result;
  result.x = x - _v2i.x;
  result.y = y - _v2i.y;
  return result;
}

v2i v2i::operator- (){
  v2i result;
  result.x = -x;
  result.y = -y;
  return result;
}

void v2i::operator-= (const v2i &_v2i){
  x -= _v2i.x;
  y -= _v2i.y;
}

bool v2i::operator== (const v2i &_v2i){
  bool is_equal_x = (x == _v2i.x);
  bool is_equal_y = (y == _v2i.y);

  if(is_equal_x && is_equal_y){
    return true;
  }

  return false;
}

bool v2i::operator!= (const v2i &_v2i){
  bool is_equal_x = (x == _v2i.x);
  bool is_equal_y = (y == _v2i.y);

  if(is_equal_x && is_equal_y){
    return false;
  }

  return true;
}

v2i v2i::operator* (const v2i &_v2i){
  v2i result;
  result.x = x * _v2i.x;
  result.y = y * _v2i.y;
  return result;
}

v2i v2i::operator* (const float &val){
  v2i result;
  result.x = x * val;
  result.y = y * val;
  return result;
}

void v2i::operator*= (const float &val){
  x *= val;
  y *= val;
}

std::ostream &operator<<(std::ostream &output, const v2i &_v2i){
  output<< "(" << _v2i.x << ", " << _v2i.y << ")";
  return output;
}

float to_degre(float radian){
  return (180.0f / PI) * radian;
}

float to_radian(float degre){
  return (degre * PI) / 180.0f;
}

float get_azimuth(v2 start, v2 target){
  float rx = target.x - start.x;
  float ry = target.y - start.y;

  float alpha = atan(rx / ry);

  bool is_x_positive = (rx >= 0);
  bool is_x_negative = (rx < 0);
  bool is_y_positive = (ry >= 0);
  bool is_y_negative = (ry < 0);

  if(is_x_positive && is_y_positive){
    return 90.0 + ( 90.0 - to_degre(alpha) );
  }
  else if(is_x_positive && is_y_negative){
    return -to_degre(alpha);
  }
  else if(is_x_negative && is_y_negative){
    return 270.0 + ( 90.0 - to_degre(alpha) );     
  }
  else if(is_x_negative && is_y_positive){
    return 180.0 + ( -to_degre(alpha) );     
  }
    
  return 0;
}

float get_distance(v2 start, v2 target){
  float rx = target.x - start.x;
  float ry = target.y - start.y;
  float distance = sqrt( pow(rx, 2) + pow(ry, 2) );
  return distance;
}

float get_magnitude(v2 vec){
  return sqrt( pow(vec.x, 2) + pow(vec.y, 2) );
}

BASE_MATH
v2 parametric(float angle, float r){
  v2 _parametric = v2_0;
  float radian = to_radian(angle - 90.0f);
  _parametric.x = cos(radian) * r;
  _parametric.y = sin(radian) * r;
  return _parametric;
}

v2 get_parametric(v2 pivot, float angle, float r){
  v2 p = parametric(angle, r);
  v2 position = v2_0;
  position.x = pivot.x + p.x;
  position.y = pivot.y + p.y;
  return position;
}

v2 move_toward(float angle, float speed){
  return parametric(angle, speed);
}

v2 move_toward(v2 start, v2 target, float speed){
  float angle = get_azimuth(start, target);
  float distance = get_distance(start, target);
  v2 p = v2_0;

  float r = distance - speed;
  float curr_speed = 0.0f; 

  if(r > 0.0f){
    curr_speed = speed; 
  }
  else{
    curr_speed = r + speed;
  }

  if(distance > 0.0f){
    p = parametric(angle, curr_speed);
  }

  return p;
}

v2 transform_rotation(v2 position, v2 pivot, float angle){
  float radian = to_radian(angle);
  float x = 0.0f;
  float y = 0.0f;
  float px = pivot.x;
  float py = pivot.y;
  float tx = position.x;
  float ty = position.y;

  x =
    (tx * cos(radian) ) -
    (ty * sin(radian) ) -
    (px * cos(radian) ) +
    (py * sin(radian) ) +
    px; 

  y =
    (tx * sin(radian) ) +
    (ty * cos(radian) ) -
    (px * sin(radian) ) -
    (py * cos(radian) ) +
    py; 

  return (v2){x, y}; 

}

v2 clamp(v2 value, v2 min, v2 max){
   v2 result = value;
   result.x = (result.x > max.x)? max.x : result.x;
   result.x = (result.x < min.x)? min.x : result.x;
   result.y = (result.y > max.y)? max.y : result.y;
   result.y = (result.y < min.y)? min.y : result.y;
   return result;
}

v2* create_v2(float x, float y){
  v2 *_v2 = (v2*)malloc(sizeof(v2));
  _v2->x = x;
  _v2->y = y;
  return _v2;
}

v2i* create_v2i(int x, int y){
  v2i *_v2i = (v2i*)malloc(sizeof(v2));
  _v2i->x = x;
  _v2i->y = y;
  return _v2i;
}

Transform* create_transform(v2 position, v2 scale, float rotation){
  Transform *transform = (Transform*)malloc(sizeof(Transform));
  transform->position = position;
  transform->rel_position = v2_0;
  transform->scale = scale;
  transform->rotation = rotation;
  transform->rel_rotation = 0.0f;
  return transform;
}
