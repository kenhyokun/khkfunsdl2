/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_audio.h"

namespace base_audio{
  static int mixing_channel_count = 0;
};

AudioSample* load_audio_sample(const char *path){
  AudioSample *sample = Mix_LoadWAV(path);

  if(!sample){
    printf("Failed to load audio sample: %s \n", Mix_GetError());
  }

  return sample;
}

AudioSource* create_audio_source(AudioSample *sample){
  AudioSource *audio_source = (AudioSource*)malloc(sizeof(AudioSource));
  audio_source->sample = sample;
  audio_source->channel = base_audio::mixing_channel_count;
  ++base_audio::mixing_channel_count;
  return audio_source;
}

void play_audio(AudioSource* audio_source, AudioPlayMode mode){
  Mix_PlayChannel(audio_source->channel, audio_source->sample, mode);
}

void pause_audio(AudioSource *audio_source){
  // if(Mix_Playing(audio_source->channel))
  Mix_Pause(audio_source->channel);
}

void resume_audio(AudioSource *audio_source){
  // if(Mix_Paused(audio_source->channel))
  Mix_Resume(audio_source->channel);
}

void stop_audio(AudioSource *audio_source){
  Mix_HaltChannel(audio_source->channel);
}

void set_audio_volume(AudioSource *audio_source, int volume){
  int max_volume = 128;
  int curr_volume = (volume * max_volume) / 100;
  Mix_Volume(audio_source->channel, curr_volume);
}

void set_master_volume(int volume){
  int max_volume = 128;
  int curr_volume = (volume * max_volume) / 100;
  Mix_Volume(-1, curr_volume);
}

bool is_audio_played(AudioSource *audio_source){
  if( Mix_Playing(audio_source->channel) ) return true;
  return false;
}

bool is_audio_paused(AudioSource *audio_source){
  if( Mix_Paused(audio_source->channel) ) return true;
  return false;
}

int get_mixing_channel_count(){
  return base_audio::mixing_channel_count;
}
