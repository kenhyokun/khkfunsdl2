/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_MATH
#define BASE_MATH

#define PI 3.14159

#include<iostream>
#include<cmath>

inline float max(float a, float b){
  return a > b ? a : b;
}

inline float min(float a, float b){
  return a < b ? a : b;
}

struct v2{
  float x;
  float y;

  v2 operator+ (const v2 &_v2);
  v2 operator- (const v2 &_v2);
  v2 operator- ();
  void operator+= (const v2 &_v2);
  void operator-= (const v2 &_v2);
  v2 operator* (const v2 &v2);
  v2 operator* (const float &val);
  void operator*= (const float &val);
  bool operator== (const v2 &_v2);
  bool operator!= (const v2 &_v2);

  friend std::ostream &operator<<(std::ostream &output, const v2 &_v2);
};

#define v2_0 (v2){0.0f, 0.0f}
#define v2_1 (v2){1.0f, 1.0f}

struct v2i{
  int x;
  int y;

  v2i operator+ (const v2i &_v2i);
  v2i operator- (const v2i &_v2i);
  v2i operator- ();
  void operator+= (const v2i &_v2i);
  void operator-= (const v2i &_v2i);
  v2i operator* (const v2i &v2i);
  v2i operator* (const float &val);
  void operator*= (const float &val);
  bool operator== (const v2i &_v2i);
  bool operator!= (const v2i &_v2i);

  friend std::ostream &operator<<(std::ostream &output, const v2i &_v2i);
};

#define v2i_0 (v2i){0, 0}
#define v2i_1 (v2i){1, 1}

v2* create_v2(float x, float y);
v2i* create_v2i(int x, int y);

struct Transform{
  v2 position;
  v2 rel_position;
  v2 scale;
  float rotation;
  float rel_rotation;
};

Transform* create_transform(v2 position = v2_0,
			    v2 scale = v2_1,
			    float rotation = 0);

float to_degre(float radian);
float to_radian(float degre);
float get_azimuth(v2 start, v2 target);
float get_distance(v2 start, v2 target);
float get_magnitude(v2 vec);
v2 get_parametric(v2 pivot, float angle, float r);
v2 move_toward(float angle, float speed);
v2 move_toward(v2 start, v2 target, float speed);
v2 transform_rotation(v2 position, v2 pivot, float angle);
v2 clamp(v2 value, v2 min, v2 max);

#endif
