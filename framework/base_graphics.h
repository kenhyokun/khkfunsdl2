/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_GRAPHICS
#define BASE_GRAPHICS

#include<iostream>
#include<cstdlib>

#include "base_sdl2_include.h"
#include "base_math.h"
#include "base_shape.h" 
#include "base_lua.h"
#include "base_coordinate.h"
#include "base_string.h"

#define LIGHTGRAY (SDL_Color){ 200, 200, 200, 255 } // Light Gray
#define GRAY (SDL_Color){ 130, 130, 130, 255 } // Gray
#define DARKGRAY (SDL_Color){ 80, 80, 80, 255 } // Dark Gray
#define YELLOW (SDL_Color){ 253, 249, 0, 255 } // Yellow
#define GOLD (SDL_Color){ 255, 203, 0, 255 } // Gold
#define ORANGE (SDL_Color){ 255, 161, 0, 255 } // Orange
#define PINK (SDL_Color){ 255, 109, 194, 255 } // Pink
#define RED (SDL_Color){ 230, 41, 55, 255 } // Red
#define MAROON (SDL_Color){ 190, 33, 55, 255 } // Maroon
#define GREEN (SDL_Color){ 0, 228, 48, 255 } // Green
#define LIME (SDL_Color){ 0, 158, 47, 255 } // Lime
#define DARKGREEN (SDL_Color){ 0, 117, 44, 255 } // Dark Green
#define SKYBLUE (SDL_Color){ 102, 191, 255, 255 } // Sky Blue
#define BLUE (SDL_Color){ 0, 0, 255, 255 } // Blue
#define DARKBLUE (SDL_Color){ 0, 82, 172, 255 } // Dark Blue
#define PURPLE (SDL_Color){ 200, 122, 255, 255 } // Purple
#define VIOLET (SDL_Color){ 135, 60, 190, 255 } // Violet
#define DARKPURPLE (SDL_Color){ 112, 31, 126, 255 } // Dark Purple
#define BEIGE (SDL_Color){ 211, 176, 131, 255 } // Beige
#define BROWN (SDL_Color){ 127, 106, 79, 255 } // Brown
#define DARKBROWN (SDL_Color){ 76, 63, 47, 255 } // Dark Brown
#define WHITE (SDL_Color){ 255, 255, 255, 255 } // White
#define BLACK (SDL_Color){ 0, 0, 0, 255 } // Black
#define BLANK (SDL_Color){ 0, 0, 0, 0 } // Blank (Transparent)
#define MAGENTA (SDL_Color){ 255, 0, 255, 255 } // Magenta
#define RAYWHITE (SDL_Color){ 245, 245, 245, 255 } // raylib White

typedef SDL_Renderer Renderer;
typedef SDL_Texture Texture;
typedef SDL_Surface Surface;
typedef SDL_Color Color;

using std::cout;
using std::endl;

void set_renderer_background_color(SDL_Renderer *renderer, SDL_Color color);

// primitive shape
enum DrawMode{
  DrawMode_fill,
  DrawMode_draw
};

void draw_line(SDL_Renderer *renderer,
	       float x1,
	       float y1,
	       float x2,
	       float y2,
	       SDL_Color color);

void draw_line(SDL_Renderer *renderer,
	       v2 point1,
	       v2 point2,
	       SDL_Color color);

void draw_line(SDL_Renderer *renderer,
	       Transform *transform1,
	       Transform *transform2,
	       SDL_Color color);

void draw_rectangle(SDL_Renderer *renderer,
		    Rectangle *rectangle,
		    SDL_Color color,
		    DrawMode mode = DrawMode_fill);

void draw_rectangle(SDL_Renderer *renderer,
		    float width,
		    float heght,
		    v2 position,
		    SDL_Color color,
		    DrawMode mode = DrawMode_fill);

void draw_circle(SDL_Renderer *renderer,
		 Circle *circle,
		 SDL_Color color,
		 DrawMode mode = DrawMode_fill);

void draw_circle(SDL_Renderer *renderer,
		 float radius,
		 v2 position,
		 SDL_Color color,
		 DrawMode mode = DrawMode_fill);

// texture
SDL_Surface* load_surface(const char *path);
SDL_Surface* copy_surface(SDL_Surface* surface, SDL_Rect *sub_rect = nullptr );

SDL_Texture* create_texture(SDL_Renderer *renderer,
			    SDL_Surface *surface,
                            int flag = 0); // use -1 to keep surface, >= 0 to free surface

SDL_Texture* create_texture(SDL_Renderer *renderer,
			    int width,
			    int height);

SDL_Texture* load_texture(SDL_Renderer *renderer, const char *path);

void draw_texture(SDL_Renderer *renderer,
		  SDL_Texture *texture,
		  int width,
		  int height,
		  float pos_x,
		  float pos_y);

// sprite
struct Sprite{
  SDL_Texture *texture;
  bool is_visible;
  int width;
  int height;
  v2i* pivot;
};

enum AnimPlay{
  AnimPlay_loop,
  AnimPlay_once
};

struct SpriteAnimator{
  Sprite *sprite;
  int frame_width;
  int frame_height;
  int sprite_column;
  int sprite_row;

  int frame_index;
  int *last_frame_list;
  double ticks;
  SDL_Rect sub_rect;
};

Sprite* create_sprite(SDL_Texture *texture);
void free_sprite(Sprite *sprite);

void draw_sprite(SDL_Renderer *renderer,
		 Sprite *sprite,
		 float pos_x,
		 float pos_y,
		 SDL_Rect *sub_rect = nullptr);

void draw_sprite(SDL_Renderer *renderer,
		 Sprite *sprite,
		 Transform *transform = nullptr,
		 SDL_Rect *sub_rect = nullptr);

SpriteAnimator* create_sprite_animator(Sprite *sprite,
				       v2i frame_size);

void run_sprite_animator(SpriteAnimator *sprite_animator,
			 int *frame_list,
			 int frame_count,
			 int fps,
			 AnimPlay anim_play = AnimPlay_loop);

void draw_sprite_animator(SDL_Renderer *renderer,
			  SpriteAnimator *sprite_animator,
			  float pos_x,
			  float pos_y);

void draw_sprite_animator(SDL_Renderer *renderer,
			  SpriteAnimator *sprite_animator,
			  Transform *transform);

// tiledmap
struct Tileset{
  int tileset_index;
  Sprite *sprite;
  int tile_width;
  int tile_height;
  int first_grid;
  int tile_count;
};

struct TiledmapLayer{
  const char *name;
  int **map_arr;
  int map_size;
}; 

struct Tiledmap{
  int column; // map width
  int row; // map height
  const char *orientation;
  int tile_width;
  int tile_height;
  int tileset_count;
  int layer_count;
  v2i max_draw_size; // x = column, y = row
  
  Tileset **tileset_list;
  TiledmapLayer **layer_list;
};

Tiledmap* create_tiledmap(SDL_Renderer *renderer,
			  const char *path,
			  char *root_path = nullptr,
			  Tileset **tileset_list = nullptr,
			  int tileset_count = 0);

void print_tiledmap_info(Tiledmap *tiledmap);

v2 get_tiledmap_grid_position(Tiledmap *tiledmap,
		     int column,
		     int row,
		     v2 position = v2_0);

v2i get_tiledmap_grid(Tiledmap *tiledmap,
	     v2 position,
	     v2 tiledmap_position = v2_0);

bool is_tiledmap_tiled(Tiledmap *tiledmap,
	      int layer_index,
	      int column,
	      int row);

void draw_tiledmap(SDL_Renderer *renderer,
		   Tiledmap *tiledmap,
		   float pos_x,
		   float pos_y,
		   int layer_index = -1);

void draw_tiledmap(SDL_Renderer *renderer,
		   Tiledmap *tiledmap,
		   Transform *transform,
		   int layer_index = -1);

// texture atlas
struct TextureAtlasPack{
  const char *name; // malloc
  SDL_Surface *surface;
};

struct TextureAtlas{
  TextureAtlasPack **pack_list;
  int pack_count;
};

TextureAtlas* load_texture_atlas(const char *path);
SDL_Surface* copy_texture_atlas_surface(TextureAtlas *texture_atlas, const char *name);
SDL_Surface* copy_texture_atlas_surface(TextureAtlas *texture_atlas, int index);
void free_texture_atlas(TextureAtlas *texture_atlas);

// TTF
TTF_Font* load_font(const char *path, int size);

void draw_text(SDL_Renderer *renderer,
	       TTF_Font *font,
	       const char *text,
	       float pos_x,
	       float pos_y,
	       SDL_Color fg_color = BLACK,
	       SDL_Color bg_color = BLANK);


// content view
struct ContentView{
  SDL_Texture *texture;
  float pos_x;
  float pos_y;
  int width;
  int height;
};

ContentView* create_content_view(SDL_Renderer *renderer,
				 float pos_x,
				 float pos_y,
				 int width,
				 int height);

void start_target_content_view(SDL_Renderer *renderer, ContentView *content_view);
void end_target_content_view(SDL_Renderer *renderer, ContentView *content_view);

// texture mod
void surface_mod_pixel_color(SDL_Surface *surface, SDL_Color color);
void texture_mod_color(SDL_Texture *texture, SDL_Color color);
void texture_mod_alpha(SDL_Texture *texture, Uint8 alpha);

inline v2i* get_top_left_pivot(){
  return create_v2i(0, 0);
}

inline v2i* get_top_right_pivot(Sprite *sprite){
  return create_v2i(sprite->width, 0);
}

inline v2i* get_bottom_right_pivot(Sprite *sprite){
  return create_v2i(sprite->width, sprite->height);
}

inline v2i* get_bottom_left_pivot(Sprite *sprite){
  return create_v2i(0, sprite->height);
}

#endif
