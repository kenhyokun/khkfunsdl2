/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_VECTOR
#define BASE_VECTOR

#include<iostream>
#include<type_traits>

inline void vector_assert(bool arg, int index, int size){
  if(arg != true){
    std::cout<<"\nTerminate called after throwing an instance of 'out_of_range'\n"
	     <<"what(): fun::vector::_M_range_check: __n\n"
	     <<"(which is "<<index<<") >= this->size() (which is "<<size<<")"
	     <<std::endl;
    exit(1);
  };
}

namespace fun{

  template<typename T>
  struct vector{
    T *t;
    int size;

    // iterator
    T* begin();
    T* end();

    T operator[] (const int &index);
    T at(int index);
    void push_back(T value);
    int find(T value);

    // just erase and clear vector element function without free memory allocation of element
    void erase(int index);
    void erase(T* iterator);
    void clear();

    // erase and clear vector element function with free memory allocation of element
    void erase_and_free_alloc(int index);
    void erase_and_free_alloc(T* iterator);
    void clear_and_free_alloc();
  };

} // namespace

template<typename T>
inline T* fun::vector<T>::begin(){
  return t;
}

template<typename T>
inline T* fun::vector<T>::end(){
  return  t + (size - 1) ;
}

template<typename T>
inline T fun::vector<T>::operator[] (const int &index){
  vector_assert(index < size, index, size);
  return t[index];
}

template<typename T>
inline T fun::vector<T>::at(int index){
  vector_assert(index < size, index, size);
  return t[index];
}

template<typename T>
inline void fun::vector<T>::push_back(T value){
  t[size] = value;
  size_t realloc_size = size + 2;
  T* realloc_t = (T*)realloc(t, realloc_size * sizeof(T));

  if(realloc_t){
    t = realloc_t;
  }
  else{
    free(t);
    exit(1);
  }

  ++size;
}

template<typename T>
inline int fun::vector<T>::find(T value) {
  for(int i = 0; i < size; ++i){
    if(at(i) == value){
      return  i;
    }
  }
  return -1;
}

template<typename T>
inline void fun::vector<T>::erase(int index) {
  vector_assert(index < size, index, size);

  if constexpr(std::is_pointer<T>::value){

    for(int i = index; i < size; ++i){
	if(t[i + 1] != nullptr){
	  t[i] = t[i + 1];
      }
    } // for
  }
  else{
    for(int i = index; i < size; ++i){
	if(&t[i + 1] != nullptr){
	  t[i] = t[i + 1];
      }
    } // for
  }

  --size;

  size_t realloc_size = size + 2;
  T* realloc_t = (T*)realloc(t, realloc_size * sizeof(T));

  if(realloc_t){
    t = realloc_t;
  }
  else{
    free(t);
    exit(1);
  }
}

template<typename T>
inline void fun::vector<T>::erase(T* iterator) {
  int index = find(*iterator);
  erase(index);
}

template<typename T>
inline void fun::vector<T>::clear(){

  free(t);
  t = (T*)malloc(sizeof(T));
  size = 0;
}

template<typename T>
inline void fun::vector<T>::erase_and_free_alloc(int index) {
  vector_assert(index < size, index, size);

  if constexpr(std::is_pointer<T>::value){
    free( t[index] );

    for(int i = index; i < size; ++i){
	if(t[i + 1] != nullptr){
	  t[i] = t[i + 1];
      }
    } // for
  }
  else{
    for(int i = index; i < size; ++i){
	if(&t[i + 1] != nullptr){
	  t[i] = t[i + 1];
      }
    } // for
  }

  --size;

  size_t realloc_size = size + 2;
  T* realloc_t = (T*)realloc(t, realloc_size * sizeof(T));

  if(realloc_t){
    t = realloc_t;
  }
  else{
    free(t);
    exit(1);
  }
}

template<typename T>
inline void fun::vector<T>::erase_and_free_alloc(T* iterator) {
  int index = find(*iterator);
  erase_and_free_alloc(index);
}

template<typename T>
inline void fun::vector<T>::clear_and_free_alloc(){

  if constexpr (std::is_pointer<T>::value ){
    for(int i = 0; i < size; ++i){
      free( t[i] );
    }
  }

  free(t);
  t = (T*)malloc(sizeof(T));
  size = 0;
}

template<typename T>
inline fun::vector<T> create_vector(){
  fun::vector<T> vector;
  vector.t = (T*)malloc(sizeof(T));
  vector.size = 0;
  return vector;
}

#endif
