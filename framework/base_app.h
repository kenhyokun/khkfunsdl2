/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_APP
#define BASE_APP

#include<iostream>

#include "base_sdl2_include.h"
#include "base_graphics.h"
#include "base_audio.h"
#include "base_input.h"
#include "base_vector.h"
#include "base_coordinate.h"
#include "base_resource_manager.h"

#define SCALE_FILTER_NEAREST (const char*) "nearest"
#define SCALE_FILTER_LINEAR (const char*) "linear"
#define SCALE_FILTER_BEST (const char*) "best"

using std::cout;
using std::endl;

void create_app(int _window_width,
		int _window_height,
		const char *title = "SDL2 Window",
		bool is_resizable = true,
		const char *scale_filter = SCALE_FILTER_LINEAR);

void run_app(void (*on_update)(void), 
             void (*on_draw)(void),
	     void (*on_draw_gui)(void),
             int target_fps = 60);

v2i get_mouse_position();
v2 get_mouse_position_f();
void add_mouse_button(MouseButton *mouse_button);
void add_key_button(KeyButton *key_button);
void add_mouse_button(MouseButton *mouse_button);
void clear_app();

#endif
