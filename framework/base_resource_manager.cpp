/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_resource_manager.h"

BASE_RESOURCE_MANAGER
bool is_extension(ResourceManager *resource_manager, const char *extension){
  for(int i = 0; i < resource_manager->extension_list.size; ++i){
    if(fun::string::equal(extension, resource_manager->extension_list[i])){
      return true;
    }
  }
  return false;
}

BASE_RESOURCE_MANAGER
void get_directory(ResourceManager *resource_manager, const char *resource_path){
  for(const auto &file : directory_iterator(resource_path)){
    const char *full_path = (const char*)file.path().c_str();
    char *dir_path = fun::string::alloc_string(resource_path);

    size_t len = strlen(full_path);
    size_t index = 0;
    for(int i = len - 1; i > 0; --i){
      if(full_path[i] == '/'){
	index = i;
	break;
      }
    }

    char *file_name = fun::string::substr(full_path, index + 1, len - index);

    if(fun::string::find(file_name, '.') == STRING_NPOS){
      fun::string::append(dir_path, file_name);
      fun::string::append(dir_path, "/");

      get_directory(resource_manager, dir_path);

      free(dir_path);
      free(file_name);

    }
    else{
      size_t index = fun::string::find(file_name, '.');
      size_t len = strlen(file_name);
      char *name = fun::string::substr((const char*)file_name, 0, index);
      char *extension = fun::string::substr((const char*)file_name, index, len - index);

      ResourceData *resource_data = (ResourceData*)malloc(sizeof(ResourceData));

      if(resource_manager->extension_list.t){
	if(is_extension(resource_manager, extension)){
	  resource_data->name = name;
	  resource_data->file_name = file_name;
	  resource_data->dir_path = dir_path;
	  resource_data->extension = extension;
	  resource_manager->resource_data_list.push_back(resource_data);
	}
	else{
	  free(name);
	  free(file_name);
	  free(dir_path);
	  free(extension);
	  free(resource_data);
	}
      }
      else{
	  resource_data->name = name;
	  resource_data->file_name = file_name;
	  resource_data->dir_path = dir_path;
	  resource_data->extension = extension;
	  resource_manager->resource_data_list.push_back(resource_data);
      }
    }
  }
}


ResourceManager* create_resource_manager(const char *src_file){

  ResourceManager *resource_manager = (ResourceManager*)malloc(sizeof(ResourceManager));
  resource_manager->src_file = src_file;

  LuaState *L = create_lua_state();

  if( load_lua_file(L, src_file) ){

    lua_getglobal(L, "extension");

    if( lua_isnil(L, -1) ){
      lua_pop(L, 1);
    }
    else{
      resource_manager->extension_list = create_vector<const char*>();

      lua_pushnil(L);
      lua_gettable(L, -2);

      while(lua_next(L, -2)){
	const char *temp_cstr = lua_tostring(L, -1);
	size_t len = strlen(temp_cstr);
	char *extension = fun::string::alloc_string(temp_cstr);
	resource_manager->extension_list.push_back(extension);
	lua_pop(L, 1);
      }
    }

    const char *resource_path = get_lua_cstr(L, "resources");

    if(resource_path){
      resource_manager->root_path = fun::string::alloc_string(resource_path);
      resource_manager->resource_data_list = create_vector<ResourceData*>();
      get_directory(resource_manager, resource_path); 
    }

  }

  lua_close(L);

  return resource_manager;

}

void add_resource(ResourceManager *resource_manager, const char *key){
  LuaState *L = create_lua_state();

  if( load_lua_file(L, resource_manager->src_file) ){

    const char *resource_path = get_lua_cstr(L, key);

    if(resource_path){
      get_directory(resource_manager, resource_path); 
    }

  }

  lua_close(L);
}

ResourceData* ResourceManager::get_data(const char *name){
  for(int i = 0; i < resource_data_list.size; ++i){
    if( fun::string::equal(resource_data_list[i]->name, name) ){
      return resource_data_list[i];
    }
  }
  return nullptr;
}

const char* ResourceManager::get_path(const char *name){
  ResourceData *data = get_data(name);
  fun::string dir_path = create_string(data->dir_path);
  fun::string file_name = create_string(data->file_name);
  dir_path += file_name;
  free((char*)file_name.c_str);
  return dir_path.c_str;
}
