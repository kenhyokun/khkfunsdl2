/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_coordinate.h"

Viewport *screen_viewport;
Viewport *world_viewport;

Viewport* create_viewport(int left, int right, int bottom, int top){
  Viewport *viewport = (Viewport*)malloc(sizeof(Viewport));
  viewport->left = left;
  viewport->right = right;
  viewport->bottom = bottom;
  viewport->top = top;
  return viewport;
}

void Viewport_Init(int logical_screen_width, int logical_screen_height){ // init on base_app.cpp
  // screen_viewport = create_viewport(0, logical_screen_width, logical_screen_height, 0); // inverse y
  screen_viewport = create_viewport(0, logical_screen_width, 0, logical_screen_height);
  world_viewport = create_viewport(0, logical_screen_width, 0, logical_screen_height);
}

v2i get_screen_position(float pos_x, float pos_y){
  int x = 0;
  int y = 0;
  
  int screen_width = screen_viewport->right - screen_viewport->left;
  int world_width = world_viewport->right - world_viewport->left;
  int a = screen_width / world_width;
  x = (a * pos_x) - ((a * world_viewport->left) - screen_viewport->left);

  int screen_height = screen_viewport->top - screen_viewport->bottom;
  int world_height = world_viewport->top - world_viewport->bottom;
  int b = screen_height / world_height;
  y = (b * pos_y) - ((b * world_viewport->bottom) - screen_viewport->bottom);

  return (v2i){x, y};
}

v2 get_world_position(int pos_x, int pos_y){
  float x = 0.0f;
  float y = 0.0f;

  int screen_width = screen_viewport->right - screen_viewport->left;
  int world_width = world_viewport->right - world_viewport->left;
  float a = (float)screen_width / (float)world_width;
  x = (pos_x + ((a * world_viewport->left) - screen_viewport->left) ) / a;

  int screen_height = screen_viewport->top - screen_viewport->bottom;
  int world_height = world_viewport->top - world_viewport->bottom;
  float b = (float)screen_height / (float)world_height;
  y = (pos_y + ((b * world_viewport->bottom) - screen_viewport->bottom) ) / b;

  return (v2){x, y};
}
