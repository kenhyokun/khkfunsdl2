/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_RESOURCE_MANAGER
#define BASE_RESOURCE_MANAGER

#include<iostream>
#include<filesystem>

#include "base_string.h"
#include "base_lua.h"
#include "base_vector.h"

using std::cout;
using std::endl;
using std::filesystem::directory_iterator;

struct ResourceData{
  const char *name;
  const char *file_name;
  const char *dir_path;
  const char *extension;
  const char *type;
};

struct ResourceManager{
  const char *src_file;
  char *root_path;
  fun::vector<const char*> extension_list;
  fun::vector<ResourceData*> resource_data_list;
  ResourceData* get_data(const char *name);
  const char* get_path(const char *name);
};

ResourceManager* create_resource_manager(const char *src_file);
void add_resource(ResourceManager *resource_manager, const char *key);

#endif

