/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include "base_input.h"

/*
  keyboard input
*/
KeyButton* create_key_button(const char *action_name,
			     SDL_Keycode key_code){
  
  KeyButton *key_button = (KeyButton*)malloc(sizeof(KeyButton));
  key_button->action_name = action_name;
  key_button->key_code = key_code;
  key_button->is_pressed = false;
  key_button->key_down_callback = nullptr;
  key_button->key_up_callback = nullptr;
  return key_button;
}

void on_key_down(SDL_Event event, KeyButton *key_button){
  if(event.key.keysym.sym == key_button->key_code){

    if(!key_button->is_pressed){
      if(key_button->key_down_callback) key_button->key_down_callback();
    }

    key_button->is_pressed = true;
  }
}

void on_key_up(SDL_Event event, KeyButton *key_button){
  if(event.key.keysym.sym == key_button->key_code){

    if(key_button->is_pressed){
      if(key_button->key_up_callback) key_button->key_up_callback();
    }

    key_button->is_pressed = false;
  }
}

/*
  mouse input
*/
MouseButton* create_mouse_button(Uint8 button_state){
  MouseButton *mouse_button= (MouseButton*)malloc(sizeof(MouseButton));
  mouse_button->button_state = button_state;
  mouse_button->is_pressed = false;
  mouse_button->button_down_callback = nullptr;
  mouse_button->button_up_callback = nullptr;
  return mouse_button;
}

void on_mouse_button_down(SDL_Event event, MouseButton *mouse_button){
  if(event.button.button == mouse_button->button_state){
    if(!mouse_button->is_pressed){
      if(mouse_button->button_down_callback) mouse_button->button_down_callback();
    }
    mouse_button->is_pressed = true;
  }
}

void on_mouse_button_up(SDL_Event event, MouseButton *mouse_button){
  if(event.button.button == mouse_button->button_state){
    if(mouse_button->is_pressed){
      if(mouse_button->button_up_callback) mouse_button->button_up_callback();
    }
    mouse_button->is_pressed = false;
  }
}
