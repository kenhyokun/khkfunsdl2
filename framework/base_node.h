/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#ifndef BASE_NODE
#define BASE_NODE

#include<iostream>

#include "base_sdl2_include.h"
#include "base_math.h"
#include "base_string.h"
#include "base_vector.h"

#define UNNAMED_NODE (const char*) "unnamed node"
#define UNTAGGED_NODE (const char*) "untagged node"

using std::cout;
using std::endl;

struct Node{
  fun::string name;
  fun::string tag;
  fun::vector<void*> component;
  fun::vector<Node*> child;
  Transform *transform;
  Node *parent;
};
  
Node* create_node(const char *name = UNNAMED_NODE,
    Transform *transform = nullptr);

void set_parent_node(Node *node, Node *_parent);
void add_child_node(Node *parent, Node *child);
void set_node_position(Node *node, v2 position);
void set_node_rotation(Node *node, float rotation);
void remove_node_child(Node *parent, Node *child);
void remove_node_child(Node *parent, int index);
void remove_node_parent(Node *node);
void remove_component(Node *node, int index);
v2 to_node_rel_position(v2 point, Node *node);
void free_node(Node *node );

#endif
